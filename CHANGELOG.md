# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


### Added

- Exception handling for run_tucan when the path is not in the repository for its current state

### Fixed

- git_last_revision output when there is no revision
- timemachine run throw the exception for a missing input file at the right moment

### Changed

[ - ]

## [0.6.0] 2024 / 07 / 30

### Added

- Whole package is now tested (functionals and units tests)

### Fixed

[ - ]

### Changed

- Some refactoring in various function that weren't doing what was expected

## [0.5.1a] 2024 / 07 / 11

### Added 

- Tests for the more atomic functions
- Functionnal tests for the basic routines involving git related commands and main_timmachine

### Fixed

[ - ]

### Changed

- Activity graphs have changed
- Various refactoring of graphs and functions to decrease overall complexity


## [0.3.0] 2023 / 01 / 25

### Added 

- Cli as now the option to take plotly
- Cli anew command to create default input file

### Fixed

[ - ]

### Changed

- Cli for timemachine imports and new that was hidden is now moved to a new command
- plot_blame_html.py now handle the reverse legend if the analyze is made by_age

## [0.2.0] 2022 / 06 / 03

### Added

- git blame information in database
- branch status information in database
- cli for join database
- names-comparator now forked
- html plots are now added for interactive view

### Fixed

- git_revision_stats now able to handle weird commits message via "latin-1" if "utf-8"  fails
- dependencies in setup.cfg to enable full use at install
- dependency on names-comparator, it is now forked and within the package
- path handling for files when sources are changing

### Changed

- activity commented in cli until it is fully implemented

## [0.1.0] 2022 / 04 / 19

### Changed

- CLI added
- commits.json features a dictionnary, not a list
- subfolders have 2 digits for monthes (2021-7 became 2021-07)
- way the joined json are written
- flatten the git_blame output dict


### Fixed

- commits.json features the author name and date of commit
- documentation explain how to use anubis

## [0.0.0] 2022 / 04 / 15

### Added

- Initial version of anubis. Run cloc, lizard and git commands

### Changed

[ - ]

### Fixed

[ - ]

### Deprecated

[ - ]
