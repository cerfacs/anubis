"""Functional tests for joindb.py"""

import os
from anubis.joindb import join_monthly_ddb

## ------ TEST_JOIN_MONTHLY_DDB ------ ##


def test_join_monthly_ddb(datadir):
    join_monthly_ddb(datadir)
    assert "joined_blame_monthly.json" in os.listdir(datadir)
