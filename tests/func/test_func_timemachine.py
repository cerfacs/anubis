"""Functional tests for timemachine.py"""

import os
import pytest
from datetime import datetime
from loguru import logger
from time import time

from anubis.timemachine import timeloop, main_timemachine, run


## ------ TEST TIMELOOP ------ ##


def mock_git_blame(path: str, print_log: bool = True):
    return [{"blame": "info"}]


def mock_git_size(path: str, previous_size: int = 0):
    return 100


@pytest.fixture
def mock_git_operations(mocker):
    # Mock necessary functions for git operations
    mocker.patch("anubis.timemachine.git_last_revision", return_value="mocked_revision")
    mocker.patch("anubis.timemachine.git_checkout")
    mocker.patch(
        "anubis.timemachine.git_revision_in_between", return_value=["rev1", "rev2"]
    )
    mocker.patch(
        "anubis.timemachine.git_revision_stats", return_value={"stats": "mock_stats"}
    )
    mocker.patch("anubis.timemachine.git_blame", side_effect=mock_git_blame)
    mocker.patch(
        "anubis.timemachine.git_branch_status", return_value={"branch": "info"}
    )
    mocker.patch("anubis.timemachine.git_size", side_effect=mock_git_size)
    mocker.patch("anubis.timemachine.run_tucan", return_value={})
    mocker.patch("anubis.timemachine.rearrange_tucan_complexity_db", return_value={})
    mocker.patch("anubis.timemachine.git_tag_history", return_value=["tag1", "tag2"])


def test_timeloop_in_future(tmpdir, mocker, mock_git_operations):

    # Mock datetime to control current date
    mock_datetime = mocker.patch("anubis.timemachine.datetime")
    mock_datetime.today.return_value = datetime(2021, 1, 1)
    mock_datetime.side_effect = lambda *args, **kwargs: datetime(*args, **kwargs)

    # Mock time to control time-based operations
    mock_time = mocker.patch("anubis.timemachine.time")
    mock_time.side_effect = time

    # Mock the logger to capture log messages
    mock_logger_info = mocker.patch("anubis.timemachine.logger.info")

    # Run the timeloop function
    timeloop(
        branch="main",
        path="src",
        year_start=2020,
        year_end=2021,  # A year in the future
        out_dir=tmpdir,
        mandatory_patterns=["*src*", "*src2*", "*src3*"],
    )

    # Check if the log message for stopping at future date is present
    mock_logger_info.assert_any_call(
        "Timemachine won't go in the future ... Stopping there"
    )


def test_timeloop(tmp_path, mock_git_operations):
    # Call the function under test
    timeloop(
        "main",
        "src",
        2020,
        2020,
        tmp_path,
        mandatory_patterns=["*src*", "*src2*", "*src3*"],
    )


## ------ TEST MAIN_TIMEMACHINE ------ ##


@pytest.mark.parametrize(
    "dir_exists, git_status_output, should_create_dir, should_call_timeloop",
    [
        (True, "", False, True),  # Directory exists, Repo is clean
        (False, "", True, True),  # Directory does not exist, Repo is clean
        (True, " M modified_file", False, False),  # Directory exists, Repo is not clean
    ],
)
def test_main_timemachine(
    mocker,
    tmp_path,
    dir_exists,
    git_status_output,
    should_create_dir,
    should_call_timeloop,
):

    mocker.patch("os.chdir")
    git_path = str(tmp_path / "git_repo")
    os.makedirs(git_path)

    out_dir = str(tmp_path / "ANUBIS_OUT")

    # Mock os.path.isdir to return the desired value
    mock_isdir = mocker.patch("os.path.isdir", return_value=dir_exists)
    mock_makedirs = mocker.patch("os.makedirs")

    # Mock subprocess.run to return the git status output
    mock_subprocess = mocker.patch(
        "subprocess.run", return_value=mocker.Mock(stdout=git_status_output.encode())
    )

    # Mock the timeloop function
    mock_timeloop = mocker.patch("anubis.timemachine.timeloop")

    main_timemachine(
        git_path,
        "main",
        "src",
        2023,
        2023,
        out_dir,
        mandatory_patterns=["*src*", "*src2*", "*src3*"],
        forbidden_patterns=[],
    )

    if should_create_dir:
        mock_makedirs.assert_called_once_with(out_dir)
    else:
        mock_makedirs.assert_not_called()

    if should_call_timeloop:
        mock_timeloop.assert_called_once_with(
            "main",
            "src",
            2023,
            2023,
            out_dir,
            mandatory_patterns=["*src*", "*src2*", "*src3*"],
            forbidden_patterns=[],
        )
    else:
        mock_timeloop.assert_not_called()

    # Ensure subprocess.run was called with the correct parameters
    mock_subprocess.assert_called_once_with(
        ["git", "status", "--porcelain"], capture_output=True
    )


## ------ TEST RUN ------ ##


def test_run_valid_inputfile(datadir):
    # General run with dummy input file
    run(os.path.join(datadir, "anubis_timemachine_test.yml"))


def test_run_file_not_found(
    datadir,
):
    # Run with non-existing file
    with pytest.raises(FileNotFoundError):
        run(os.path.join(datadir, "non_existent.yml"))
