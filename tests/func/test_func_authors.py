"""Functional tests for authors.py"""

import os

from anubis.authors import get_author_list, build_aliases

## ------ TEST_GET_AUTHOR_LIST ------- ##


def test_get_author_list(datadir, tmpdir):
    result = get_author_list(datadir, filename=f"{tmpdir}/authors_raw.json")

    assert "authors_raw.json" in os.listdir(tmpdir)
    assert result == ["User1", "User2", "User3"]


## ------ TEST_BUILD_ALIASES ------ ##


def test_build_aliases():
    authors = [
        "Jean Dupont",
        "Jean Dupont",
        "Jean Dupont",
        "Jean Dupont",
        "Jean Dupont",
        "Jean Dupont",
        "Karl31",
        "Karl31",
        "Karl31",
        "Nicolas Dubois",
    ]

    assert build_aliases(authors) == {
        "Jean Dupont": "JDT",
        "Karl31": "KAR",
        "Nicolas Dubois": "NDS",
    }


def test_build_aliases_matching_authors():
    authors = [
        "Jean Dupont",
        "jean dupont",
        "jean dupont",
        "Karl31",
        "Nicolas Dubois",
    ]

    assert build_aliases(authors) == {
        "Jean Dupont": "JDT",
        "jean dupont": "JDT",
        "Karl31": "KAR",
        "Nicolas Dubois": "NDS",
    }


def test_build_aliases_multiple_matching():
    authors = [
        "Jean Dupont",
        "jean dupont",
        "jean dupont",
        "Karl31",
        "karl31",
        "Nicolas Dubois",
    ]

    assert build_aliases(authors) == {
        "Jean Dupont": "JDT",
        "jean dupont": "JDT",
        "Karl31": "KAR",
        "karl31": "KAR",
        "Nicolas Dubois": "NDS",
    }


def test_build_aliases_matching_different_names():
    authors = [
        "Karl31",
        "Jean Dupont",
        "Raphael Lain",
        "karl31",
        "Raphael Dupont",
        "Raphaël",
    ]

    result = build_aliases(authors)
    assert result == {
        "Karl31": "KAR",
        "Jean Dupont": "JDT",
        "Raphael Lain": "RLN",
        "karl31": "KAR",
        "Raphael Dupont": "RLN",
        "Raphaël": "RLN",
    }
