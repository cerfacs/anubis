"""Functionals tests for plot_activity.py"""

from anubis.plot_activity import (
    plot_global_additions,
    plot_activity_cumulated,
    plot_activity_authors,
)


## ------ TEST PLOT_GLOBAL_ADDITIONS ------ ##


def test_plot_global_additions(datadir):
    ax = plot_global_additions(folder=datadir)

    assert ax.get_title() == "Global Addition Rate"

    assert ax.get_xlabel() == "Commits Day"
    assert ax.get_ylabel() == "Additions"


## ------ TEST PLOT_ACTIVITY_CUMULATED ------ ##


def test_plot_activity_cumulated(datadir):
    ax = plot_activity_cumulated(
        "date", "insertions", "Super Title", "y_axis", folder=datadir
    )

    assert ax.get_title() == "Super Title"
    assert ax.get_ylabel() == "y_axis"


## ------ TEST PLOT_ACTIVITY_AUTHORS ------ ##


def test_plot_activity_authors(datadir):
    ax = plot_activity_authors("time", "auth", "y_label", folder=datadir)

    assert ax.get_title() == "Authors activity"
    assert ax.get_ylabel() == "y_label"
