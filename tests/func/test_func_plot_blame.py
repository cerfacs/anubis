"""Functionals tests for plot_blame.py"""

import pytest

from anubis.plot_blame import plot_birth, plot_ownership


## ------ TEST PLOT_BIRTH ------ ##


@pytest.mark.parametrize(
    "by_age,title",
    [
        (True, "Code lines gathered by time since last edition"),
        (False, "Code lines gathered by date of last edition"),
    ],
)
def test_plot_birth(datadir, by_age, title):
    ax = plot_birth(folder=datadir, by_age=by_age)

    assert ax.get_title() == title
    assert ax.get_ylabel() == "Lines of code"


## ------ TEST PLOT_OWNERSHIP ------ ##


def test_plot_ownership(datadir):
    ax = plot_ownership(folder=datadir)

    assert ax.get_title() == "Code ownership"
    assert ax.get_ylabel() == "Lines of code"
