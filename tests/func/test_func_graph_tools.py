"""Functional tests for graph_tools.py"""

import matplotlib.pyplot as plt
import plotly.graph_objects as go

from tol_colors import tol_cmap

from anubis.graph_tools import stack_plot_html, update_legend

## ------ TEST_STACK_PLOT_HTML ------ ##

data_dict = {
    "User1": {
        "dates": ["2022-01-01", "2022-02-01", "2022-03-01"],
        "commits": [5, 3, 8],
    },
    "User2": {
        "dates": ["2022-01-01", "2022-02-01", "2022-03-01"],
        "commits": [2, 6, 1],
    },
    "User3": {
        "dates": ["2022-01-01", "2022-02-01", "2022-03-01"],
        "commits": [1, 2, 3],
    },
}


def test_stack_plot_html():
    qoi_key = "commits"
    largest_contributors = 2
    ascend_order = True

    fig = stack_plot_html(
        data_dict,
        qoi_key,
        largest_contributors=largest_contributors,
        ascend_order=ascend_order,
    )

    # Check if the returned object is a go.Figure
    assert isinstance(fig, go.Figure)

    # Check the number of traces in the figure
    assert len(fig.data) == 3

    # Check trace names and data
    expected_names = ["User3", "User2", "User1"]
    for trace, expected_name in zip(fig.data, expected_names):
        assert trace.name == expected_name

    # Check annotations
    annotations = fig.layout.annotations
    assert len(annotations) == 2
    expected_annotations = ["User1", "User2"]
    for annotation, expected_text in zip(annotations, expected_annotations):
        assert (
            annotation.text == expected_text
        ), f"Annotation text {annotation.text} is incorrect, expected {expected_text}"


def test_stack_plot_html_no_largest_descending_order():
    qoi_key = "commits"
    cmap = tol_cmap("rainbow_WhBr")

    fig = stack_plot_html(data_dict, qoi_key, cmap=cmap, ascend_order=False)

    # Check if the returned object is a go.Figure
    assert isinstance(fig, go.Figure)

    # Check the number of traces in the figure
    assert len(fig.data) == 3

    # Check trace names and data
    expected_names = ["User1", "User2", "User3"]
    for trace, expected_name in zip(fig.data, expected_names):
        assert trace.name == expected_name

    # Check annotations
    annotations = fig.layout.annotations
    assert len(annotations) == 0
    assert annotations == ()


## ------ TEST_UPDATE_LEGEND ------ ##


def test_update_legend():
    # Create a sample plot
    _, ax = plt.subplots()
    ax.plot([0, 1, 2], [0, 1, 0], label="Line 1")
    ax.plot([0, 1, 2], [1, 0, 1], label="Line 2")

    # Call the update_legend function
    updated_ax = update_legend(ax)

    legend = updated_ax.get_legend()

    assert len(legend.get_texts()) == 2
    assert legend.get_texts()[0].get_text() == "Line 1"
    assert legend.get_texts()[1].get_text() == "Line 2"
