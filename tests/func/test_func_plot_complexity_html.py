"""Functional tests for plot_complexity_html.py"""

import plotly.graph_objects as go


from anubis.plot_complexity_html import (
    plot_worst_performers_html,
    plot_global_complexity_html,
)


## ------ TEST PLOT_WORST_PERFORMERS_HTML ------ ##


def test_plot_worst_performers_html(datadir):
    fig = plot_worst_performers_html(datadir, exclude_patterns=[])

    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == "Worst performers"
    assert fig.layout.xaxis.type == "date"
    assert fig.layout.yaxis.title.text == "Score - 0 (good) to 10 (bad)"


## ------ TEST PLOT_GLOBAL_COMPLEXITY_HTML ------ ##


def test_plot_global_complexity_html(datadir):
    fig = plot_global_complexity_html(datadir, exclude_patterns=[])

    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == "Global Complexity"
    assert fig.layout.xaxis.type == "date"
    assert fig.layout.yaxis.title.text == "Score - 0 (good) to 10 (bad)"
