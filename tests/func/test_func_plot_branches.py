"""Functionals tests for plot_branches.py"""

import pytest

from anubis.plot_branches import plot_worst_performers, plot_all_ahead


## ------ TEST PLOT_WORST_PERFORMERS ------ ##


@pytest.mark.parametrize("qoi_key, switch_behind", [("behind", True), ("ahead", False)])
def test_plot_worst_performers(datadir, qoi_key, switch_behind):
    ax = plot_worst_performers(
        folder=datadir, switch_behind=switch_behind, nbranches=2, exclude_patterns=[]
    )

    assert ax.get_title() == f"2 unmerged branches most {qoi_key}"
    assert ax.get_ylabel() == f"Commits {qoi_key}"


## ------ TEST PLOT_ALL_AHEAD ------ ##


def test_plot_all_ahead(datadir):
    ax = plot_all_ahead(folder=datadir, exclude_patterns=[])

    assert ax.get_title() == "Unmerged Branches contribution to commits"
    assert ax.get_ylabel() == "Number of Commits"
