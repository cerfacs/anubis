"""Functionnal test for external_analysis.py"""

import pytest

from anubis.external_analysis import run_tucan

## ------ TEST_RUN_TUCAN ------ ##
mandatory_patterns = ["*file*"]
forbidden_patterns = []


# Mock functions that run_tucan relies on that are not in Anubis
def mock_find_package_files_and_folders(
    path, mandatory_patterns=mandatory_patterns, forbidden_patterns=forbidden_patterns
):
    return ["file1.py", "file2.py", "file3.xml"]


# def mock_clean_extensions_in_paths(paths):
#     return ["file1.py", "file2.py"]


def mock_run_struct(paths, ignore_errors=True):
    return {"file1.py": "analysis1", "file2.py": "analysis2"}


# Test function
def test_run_tucan(mocker):
    # Mocking the functions used by run_tucan
    mock_rec = mocker.patch(
        "anubis.external_analysis.find_package_files_and_folders",
        side_effect=mock_find_package_files_and_folders,
    )
    mock_run = mocker.patch(
        "anubis.external_analysis.run_struct", side_effect=mock_run_struct
    )

    path = "some/path"

    result = run_tucan(
        path,
        mandatory_patterns,
        forbidden_patterns,
    )

    # Check if the returned value is as expected
    assert result == {"file1.py": "analysis1", "file2.py": "analysis2"}

    # Ensure the mocked functions were called with the expected arguments
    mock_rec.assert_called_once_with(
        path,
        mandatory_patterns=mandatory_patterns,
        forbidden_patterns=forbidden_patterns,
    )
    mock_run.assert_called_once_with(
        ["file1.py", "file2.py", "file3.xml"], ignore_errors=False
    )


def test_run_tucan_fail():
    path = "non_existent_path"
    optional_path_list = ["optional_path_1", "optional_path_2"]

    with pytest.raises(FileNotFoundError):
        run_tucan(path, optional_path_list)
