"""Functionals tests for plot_branches_html.py"""

import pytest

import plotly.graph_objects as go

from anubis.plot_branches_html import (
    plot_worst_branches_html,
    plot_global_branches_html,
)


## ------ TEST PLOT_WORST_BRANCHES_HTML ------ ##


def test_plot_worst_branches_html(datadir):
    fig = plot_worst_branches_html(folder=datadir, nbranches=2, exclude_patterns=[])

    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == f"Top 2 Worst performers by commits behind"
    assert fig.layout.xaxis.type == "date"
    assert fig.layout.yaxis.title.text == "behind"


## ------ TEST PLOT_GLOBAL_BRANCHES_HTML ------ ##


@pytest.mark.parametrize(
    "switch_behind, qoi_key",
    [(True, "behind"), (False, "ahead")],
)
def test_plot_global_branches_html(datadir, switch_behind, qoi_key):
    fig = plot_global_branches_html(
        folder=datadir, switch_behind=switch_behind, exclude_patterns=[]
    )
    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == f"Worst performers by commits {qoi_key}"
    assert fig.layout.xaxis.type == None
    assert fig.layout.yaxis.title.text == qoi_key
