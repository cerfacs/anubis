"""Functional tests for plot_complexity.py"""

from anubis.plot_complexity import plot_complexity, plot_worst_performers


## ------ TEST PLOT_COMPLEXITY ------ ##


def test_plot_complexity(datadir):
    ax = plot_complexity(datadir, exclude_patterns=[])

    assert ax.get_title() == "Global Complexity"
    assert ax.get_ylabel() == "Score - 0 (good) to 10 (bad)"


## ------ TEST PLOT_WORST_PERFORMERS ------ ##


def test_plot_worst_performers(datadir):
    ax = plot_worst_performers(datadir, exclude_patterns=[])

    assert ax.get_title() == "Worst performers"
    assert ax.get_ylabel() == "Score - 0 (good) to 10 (bad)"
