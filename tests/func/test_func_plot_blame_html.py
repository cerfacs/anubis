"""Functionals tests for plot_blame_html.py"""

import pytest

import plotly.graph_objects as go

from anubis.plot_blame_html import plot_birth_html, plot_ownership_html

## ------ TEST PLOT_BIRTH_HTML ------ ##


@pytest.mark.parametrize(
    "by_age, title",
    [
        (True, "Code lines gathered by time since last edition"),
        (False, "Code lines gathered by date of last edition"),
    ],
)
def test_plot_birth_html(datadir, by_age, title):
    fig = plot_birth_html(folder=datadir, by_age=by_age)
    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == title
    assert fig.layout.yaxis.title.text == "Lines of code"


## ------ TEST PLOT_OWNERSHIP_HTML------ ##


def test_plot_ownership_htmlr(datadir):
    fig = plot_ownership_html(folder=datadir)
    assert isinstance(fig, go.Figure)

    assert fig.layout.title.text == "Code ownership"
    assert fig.layout.yaxis.title.text == "Lines of code"
