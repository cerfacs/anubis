"""Unitary tests for joindb.py"""

from anubis.joindb import join_anubis_files

## ------ TEST_JOIN_ANUBIS_FILES ------ ##


def test_join_anubis_files(datadir):
    fname = "blame.json"
    assert join_anubis_files(datadir, fname) == {
        "2024-01": [
            {
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User3",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2020-10-10",
                ],
                "file": "dummy.py",
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
        "2024-02": [
            {
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User1",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2024-01-10",
                ],
                "file": "dummy.py",
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
    }


def test_join_anubis_files_no_file(datadir):
    fname = "commits.json"
    assert join_anubis_files(datadir, fname) == {}


def test_join_anubis_files_wrong_format(datadir):
    fname = "wrong.txt"
    assert join_anubis_files(datadir, fname) == {}
