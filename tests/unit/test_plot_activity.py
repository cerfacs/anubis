"""Unitary tests for plot_activity.py"""

import pytest
from datetime import datetime

import numpy as np

from anubis.plot_activity import (
    build_commitdays_authors,
    build_dapm,
    build_dapy,
    build_activity,
    get_mvp,
    sort_trigrams_by_selected_authors,
    clean_authors_db,
    gather_authors_by_trigram,
    merge_authors_db_by_trigram,
    process_author_data,
    process_author_data_and_mvp,
    gather_authors_activity,
)

data_by_obs_time = {
    datetime(2022, 10, 1): [
        {
            "author": "Theophile Rondinaud",
            "date": "Fri Sep 30 15:12:55 2022 +0200",
            "insertions": 10,
            "deletions": 2,
            "br_type": "dev",
        },
        {
            "author": "Theodule Brincan",
            "date": "Sat Oct 15 22:32:12 2022 +0200",
            "insertions": 5,
            "deletions": 1,
            "br_type": "dev",
        },
    ],
    datetime(2022, 11, 1): [
        {
            "author": "Theophile Rondinaud",
            "date": "Mon Oct 24 09:10:32 2022 +0200",
            "insertions": 15,
            "deletions": 5,
            "br_type": "dev",
        },
        {
            "author": "Norbert Colliou",
            "date": "Mon Oct 31 12:34:01 2022 +0200",
            "insertions": 7,
            "deletions": 0,
            "br_type": "other",
        },
    ],
    datetime(2022, 12, 1): [  # Commit with no author, this can happen
        {
            "author": "",
            "date": "Wed Nov 16 19:16:32 2022 +0200",
            "insertions": 15,
            "deletions": 5,
            "br_type": "dev",
        },
    ],
}

## ------ TEST BUILD_COMMITDAYS_AUTHORS ------ ##


def test_build_commitdays_authors():

    expected_output = {
        "Theophile Rondinaud": {
            "start_date": datetime(2022, 9, 30, 0, 0),
            "last_date": datetime(2022, 10, 24, 0, 0),
            "date": [datetime(2022, 9, 30, 0, 0), datetime(2022, 10, 24, 0, 0)],
            "age": [0, 1],
            "commits_days": [1, 1],
            "insertions": [10, 15],
            "deletions": [2, 5],
            "additions": [8, 10],
        },
        "Theodule Brincan": {
            "start_date": datetime(2022, 10, 15, 0, 0),
            "last_date": datetime(2022, 10, 15, 0, 0),
            "date": [datetime(2022, 10, 15, 0, 0)],
            "age": [0],
            "commits_days": [1],
            "insertions": [5],
            "deletions": [1],
            "additions": [4],
        },
    }

    result = build_commitdays_authors(data_by_obs_time)

    assert result == expected_output


## ------ TEST BUILD_DAPM ------ ##

trigram = {
    "theophile rondinaud": "TRD",
    "theodule brincan": "TBN",
    "norbert colliou": "NCU",
}


def test_build_dapm():

    time_axis, authors_number = build_dapm(data_by_obs_time, trigram)

    expected_time_axis = [
        datetime(2022, 10, 1),
        datetime(2022, 11, 1),
        datetime(2022, 12, 1),
    ]
    expected_authors_number = [2, 1, 1]

    assert time_axis == expected_time_axis
    assert authors_number == expected_authors_number


## ------ TEST BUILD_DAPY ------ ##


def test_build_dapy():
    year_list, dapy_list = build_dapy(data_by_obs_time, trigram)

    expected_year_list = [datetime(2022, 1, 1, 0, 0)]
    expected_dapy_list = [3]

    assert year_list == expected_year_list
    assert dapy_list == expected_dapy_list


## ------ TEST BUILD_ACTIVITY ------ ##


def test_build_activity():
    comday_data = {
        "Theophile Rondinaud": {
            "start_date": datetime(2022, 9, 30, 0, 0),
            "last_date": datetime(2022, 10, 24, 0, 0),
            "date": [datetime(2022, 9, 30, 0, 0), datetime(2022, 10, 24, 0, 0)],
            "age": [0, 1],
            "commits_days": [1, 1],
            "insertions": [10, 15],
            "deletions": [2, 5],
            "additions": [8, 10],
        },
        "Theodule Brincan": {
            "start_date": datetime(2022, 10, 15, 0, 0),
            "last_date": datetime(2022, 10, 15, 0, 0),
            "date": [datetime(2022, 10, 15, 0, 0)],
            "age": [0],
            "commits_days": [1],
            "insertions": [5],
            "deletions": [1],
            "additions": [4],
        },
    }

    months_results, activity_result = build_activity(comday_data, "commits_days")
    assert [datetime(2022, 9, 1, 0, 0), datetime(2022, 10, 1, 0, 0)] == months_results
    assert (activity_result == np.array([1, 3])).all()


## ------ TEST GET_MVP ------ ##


def test_get_mvp():
    auth_list = ["User1", "User2", "User3", "User4", "User5"]
    auth_reach = [89, 43, 45, 21, 2]

    result = get_mvp(auth_list, auth_reach)

    assert result == ["User1", "User3", "User2"]


## ------ TEST SORT_TRIGRAMS_BY_SELECTED_AUTHORS ------ ##


@pytest.mark.parametrize(
    "authors_list, expected_trigrams",
    [
        (["Karl31", "Jean Dupont"], {"karl31": "KAR", "jean dupont": "JDT"}),
        (
            ["all"],
            {
                "karl31": "KAR",
                "jean dupont": "JDT",
                "raphael lain": "RLN",
                "raphael dupont": "RLT",
                "raphaël": "RLN",
            },
        ),
    ],
)
def test_sort_trigrams_by_selected_authors(datadir, authors_list, expected_trigrams):
    result = sort_trigrams_by_selected_authors(datadir, authors_list)

    assert result == expected_trigrams


## ------ TEST CLEAN_AUTHORS_DB ------ ##


def test_clean_authors_db():
    trigram = {
        "theophile rondinaud": "TRD",
        "theodule brincan": "TBN",
    }
    result = clean_authors_db(data_by_obs_time, trigram)

    assert result == {
        "Theophile Rondinaud": {
            "additions": [8, 10],
            "age": [0, 1],
            "commits_days": [1, 1],
            "date": [
                datetime(2022, 9, 30, 0, 0),
                datetime(2022, 10, 24, 0, 0),
            ],
            "deletions": [2, 5],
            "insertions": [10, 15],
            "last_date": datetime(2022, 10, 24, 0, 0),
            "start_date": datetime(2022, 9, 30, 0, 0),
        },
        "Theodule Brincan": {
            "additions": [4],
            "age": [0],
            "commits_days": [1],
            "date": [datetime(2022, 10, 15, 0, 0)],
            "deletions": [1],
            "insertions": [5],
            "last_date": datetime(2022, 10, 15, 0, 0),
            "start_date": datetime(2022, 10, 15, 0, 0),
        },
    }


## ------ TEST GATHER_AUTHORS_BY_TRIGRAM ------ ##


def test_gather_authors_by_trigram():
    trigram = {
        "karl31": "KAR",
        "jean dupont": "JDT",
        "raphael lain": "RLN",
        "raphael dupont": "RLT",
        "raphaël": "RLN",
    }

    result = gather_authors_by_trigram(trigram)
    expected_twins = [
        ["karl31"],
        ["jean dupont"],
        ["raphael lain", "raphaël"],
        ["raphael dupont"],
    ]
    assert result == expected_twins


## ------ TEST MERGE_AUTHORS_DB_BY_TRIGRAM ------ ##
authors_data = {
    "Raphael Lain": {
        "additions": [17, 16],
        "age": [0, 4],
        "commits_days": [1, 1],
        "date": [
            datetime(2022, 4, 7, 0, 0),
            datetime(2022, 8, 6, 0, 0),
        ],
        "deletions": [3, 2],
        "insertions": [20, 18],
        "last_date": datetime(2022, 8, 6, 0, 0),
        "start_date": datetime(2022, 4, 7, 0, 0),
    },
    "Raphaël": {
        "additions": [7, 9],
        "age": [0, 3],
        "commits_days": [1, 1],
        "date": [
            datetime(2022, 4, 11, 0, 0),
            datetime(2022, 7, 5, 0, 0),
        ],
        "deletions": [1, 5],
        "insertions": [8, 14],
        "last_date": datetime(2022, 7, 5, 0, 0),
        "start_date": datetime(2022, 4, 11, 0, 0),
    },
}


def test_merge_authors_db_by_trigram():
    twins = [
        ["raphael lain", "raphaël"],
    ]
    result = merge_authors_db_by_trigram(twins, authors_data, "date")

    expected_db = {
        "Raphael Lain": {
            "additions": [17, 7, 9, 16],
            "age": [0, 0, 3, 4],
            "commits_days": [1, 1, 1, 1],
            "date": [
                datetime(2022, 4, 7, 0, 0),
                datetime(2022, 4, 11, 0, 0),
                datetime(2022, 7, 5, 0, 0),
                datetime(2022, 8, 6, 0, 0),
            ],
            "deletions": [3, 1, 5, 2],
            "insertions": [20, 8, 14, 18],
            "last_date": datetime(2022, 8, 6, 0, 0),
            "start_date": datetime(2022, 4, 7, 0, 0),
        }
    }
    assert result == expected_db


def test_merge_authors_db_by_trigram_no_merge():
    twins = [["karl31", "Karl31"]]
    result = merge_authors_db_by_trigram(twins, authors_data, "date")
    assert result == authors_data  # Result is unchanged


## ------ TEST PROCESS_AUTHORS_DATA ------ ##


def test_process_authors_data():
    reduced_authors_data = {
        "Raphael Lain": {
            "additions": [17, 7, 9, 16],
            "age": [0, 0, 3, 4],
            "commits_days": [1, 1, 1, 1],
            "date": [
                datetime(2022, 4, 7, 0, 0),
                datetime(2022, 4, 11, 0, 0),
                datetime(2022, 7, 5, 0, 0),
                datetime(2022, 8, 6, 0, 0),
            ],
            "deletions": [3, 1, 5, 2],
            "insertions": [20, 8, 14, 18],
            "last_date": datetime(2022, 8, 6, 0, 0),
            "start_date": datetime(2022, 4, 7, 0, 0),
        },
        "karl31": {
            "additions": [25, 12, 15],
            "age": [0, 3, 4],
            "commits_days": [1, 1, 1],
            "date": [
                datetime(2022, 6, 7, 0, 0),
                datetime(2022, 9, 24, 0, 0),
                datetime(2022, 10, 20, 0, 0),
            ],
            "deletions": [3, 5, 1],
            "insertions": [28, 17, 16],
            "last_date": datetime(2022, 10, 20, 0, 0),
            "start_date": datetime(2022, 6, 7, 0, 0),
        },
        "Raphael Dupont": {
            "additions": [25, 8, 9, 13, 4],
            "age": [0, 2, 3, 4, 10],
            "commits_days": [1, 1, 1, 1, 1],
            "date": [
                datetime(2022, 2, 21, 0, 0),
                datetime(2022, 5, 20, 0, 0),
                datetime(2022, 6, 24, 0, 0),
                datetime(2022, 7, 22, 0, 0),
                datetime(2022, 12, 26, 0, 0),
            ],
            "deletions": [8, 3, 5, 4, 2],
            "insertions": [33, 11, 14, 17, 6],
            "last_date": datetime(2022, 12, 26, 0, 0),
            "start_date": datetime(2022, 2, 21, 0, 0),
        },
        "Theodule Brincan": {
            "additions": [4],
            "age": [0],
            "commits_days": [1],
            "date": [datetime(2022, 10, 15, 0, 0)],
            "deletions": [1],
            "insertions": [5],
            "last_date": datetime(2022, 10, 15, 0, 0),
            "start_date": datetime(2022, 10, 15, 0, 0),
        },
        "Theophile Rondinaud": {
            "additions": [8, 10],
            "age": [0, 1],
            "commits_days": [1, 1],
            "date": [
                datetime(2022, 9, 30, 0, 0),
                datetime(2022, 10, 24, 0, 0),
            ],
            "deletions": [2, 5],
            "insertions": [10, 15],
            "last_date": datetime(2022, 10, 24, 0, 0),
            "start_date": datetime(2022, 9, 30, 0, 0),
        },
        "Jean Dupont": {
            "additions": [8, 23, 6],
            "age": [0, 2, 6],
            "commits_days": [1, 1, 1],
            "date": [
                datetime(2022, 1, 19, 0, 0),
                datetime(2022, 3, 22, 0, 0),
                datetime(2022, 7, 17, 0, 0),
            ],
            "deletions": [7, 5, 2],
            "insertions": [15, 28, 8],
            "last_date": datetime(2022, 7, 17, 0, 0),
            "start_date": datetime(2022, 1, 19, 0, 0),
        },
    }
    authors_name = ["Theophile Rondinaud", "Jean Dupont"]
    result = process_author_data(
        authors_name, reduced_authors_data, "date", "additions"
    )
    assert result == {
        "Jean Dupont": {
            "additions": [8, 31, 37],
            "date": (
                datetime(2022, 1, 19, 0, 0),
                datetime(2022, 3, 22, 0, 0),
                datetime(2022, 7, 17, 0, 0),
            ),
        },
        "Theophile Rondinaud": {
            "additions": [8, 18],
            "date": (
                datetime(2022, 9, 30, 0, 0),
                datetime(2022, 10, 24, 0, 0),
            ),
        },
    }


## ------ TEST PROCESS_AUTHORS_DATA_AND_MVP ------ ##


def test_process_author_data_and_mvp(datadir):
    authors_nomvp, authors_mvp = process_author_data_and_mvp(
        "date", "deletions", folder=datadir
    )

    assert authors_nomvp == {}
    assert authors_mvp == {
        "Jean Dupont": {
            "date": (datetime(2019, 12, 31, 0, 0),),
            "deletions": [304],
        },
        "Raphael Lain": {
            "date": (datetime(2023, 12, 31, 0, 0),),
            "deletions": [0],
        },
    }


## ------ TEST GATHER_AUTHORS_ACTIVITY ------ ##


def test_father_authors_activity(datadir):
    result = gather_authors_activity(folder=datadir)

    assert result == {
        "On Dev branch, per month": {
            "auth": [2],
            "time": [datetime(2020, 1, 1, 0, 0)],
        },
        "On Dev branch, per year": {
            "auth": [2],
            "time": [datetime(2020, 1, 1, 0, 0)],
        },
    }
