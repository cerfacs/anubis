"""Unitary tests for plot_common.py"""

from datetime import datetime
import numpy as np

from anubis.plot_common import (
    gather_data_by_category,
    gather_data_by_category_in_list,
    max_in_dicts,
    create_stack_plot_xbaseline,
    merge_data_with_baseline,
    sort_by_highest_amount,
    stack_data,
)

## ------ TEST_GATHER_DATA_BY_CATEGORY ------ ##


def test_gather_data_by_category():
    mock_dict = {
        datetime(2020, 1, 1): {
            "commits": [1, 2, 3],
            "additions": [10, 20, 30],
        },
        datetime(2020, 2, 1): {
            "commits": [6, 5, 4],
            "additions": [1, 2, 3],
        },
    }

    result = gather_data_by_category(mock_dict)
    assert result == {
        "additions": {
            "amount": [[10, 20, 30], [1, 2, 3]],
            "dates": [datetime(2020, 1, 1), datetime(2020, 2, 1)],
        },
        "commits": {
            "amount": [[1, 2, 3], [6, 5, 4]],
            "dates": [datetime(2020, 1, 1), datetime(2020, 2, 1)],
        },
    }


## ------ TEST_GATHER_DATA_BY_CATEGORY_IN_LIST ------ ##


def test_gather_data_by_category_in_list():
    mock_dict = {
        datetime(2024, 6, 1, 0, 0): {
            "branch": np.array(
                ["remotes/origin/master", "remotes/origin/release/v2.0.0"]
            ),
            "behind": np.array([6, 28]),
            "ahead": np.array([0, 0]),
            "nb_commits_ref_branch": np.array([1962, 1962]),
        }
    }
    key_to_focus = "branch"
    qoi_key = "behind"

    assert gather_data_by_category_in_list(mock_dict, key_to_focus, qoi_key) == {
        "remotes/origin/master": {"amount": [6], "dates": [datetime(2024, 6, 1, 0, 0)]},
        "remotes/origin/release/v2.0.0": {
            "amount": [28],
            "dates": [datetime(2024, 6, 1, 0, 0)],
        },
    }


## ------ TEST_MAX_IN_DICTS ------ ##


def test_max_in_dicts():
    mock_dict = {
        "Branch1": {
            "dates": [
                datetime(2024, 6, 1, 0, 0),
                datetime(2024, 5, 1, 0, 0),
                datetime(2024, 4, 1, 0, 0),
            ],
            "behind": [6, 1, 5],
            "ahead": [0, 0, 1],
            "nb_commits_ref_branch": [1962, 1957, 1939],
        },
        "Branch2": {
            "dates": [
                datetime(2024, 6, 1, 0, 0),
                datetime(2024, 5, 1, 0, 0),
                datetime(2024, 4, 1, 0, 0),
            ],
            "behind": [4, 1, 43],
            "ahead": [8, 2, 1],
            "nb_commits_ref_branch": [1962, 1957, 1939],
        },
        "Branch3": {
            "dates": [
                datetime(2024, 6, 1, 0, 0),
                datetime(2024, 5, 1, 0, 0),
                datetime(2024, 4, 1, 0, 0),
            ],
            "behind": [3, 1, 4],
            "ahead": [3, 12, 1],
            "nb_commits_ref_branch": [1962, 1957, 1939],
        },
    }

    mask_top, mask_idx = max_in_dicts(mock_dict, "ahead")
    assert mask_top == [1, 2]
    assert mask_idx == [0, 1]


## ------ TEST_CREATE_STACK_PLOT_XBASELINE ------ ##
mock_dict = {
    "commits": {
        "dates": [
            datetime(2024, 6, 1, 0, 0),
            datetime(2024, 5, 1, 0, 0),
            datetime(2024, 4, 1, 0, 0),
        ],
        "amount": [4, 3, 2],
    },
    "additions": {
        "dates": [
            datetime(2024, 8, 1, 0, 0),
            datetime(2024, 5, 1, 0, 0),
            datetime(2024, 1, 1, 0, 0),
        ],
        "amount": [40, 3, 18],
    },
}


def test_create_stack_plot_xbaseline():
    assert create_stack_plot_xbaseline(mock_dict, "dates") == [
        datetime(2024, 1, 1, 0, 0),
        datetime(2024, 4, 1, 0, 0),
        datetime(2024, 5, 1, 0, 0),
        datetime(2024, 6, 1, 0, 0),
        datetime(2024, 8, 1, 0, 0),
    ]


## ------ TEST_MERGE_DATA_WITH_BASELINE ------ ##


def test_merge_data_with_baseline():
    x_baseline = [
        datetime(2024, 1, 1, 0, 0),
        datetime(2024, 4, 1, 0, 0),
        datetime(2024, 5, 1, 0, 0),
        datetime(2024, 6, 1, 0, 0),
        datetime(2024, 8, 1, 0, 0),
    ]

    assert merge_data_with_baseline(mock_dict, x_baseline, "dates", "amount") == {
        "additions": {"amount": [18, 0, 3, 0, 40]},
        "commits": {"amount": [0, 2, 3, 4, 0]},
    }


## ------ TEST_SORT_BY_HIGHEST_AMOUNT ------ ##


def test_sort_by_highest_amount_whole():
    assert sort_by_highest_amount(mock_dict, "amount") == {
        "additions": 40,
        "commits": 4,
    }


def test_sort_by_highest_amount_last():
    assert sort_by_highest_amount(mock_dict, "amount", span="last") == {
        "additions": 18,
        "commits": 2,
    }


## ------ TEST_STACK_DATA ------ ##


def test_stack_data():
    x_baseline = [
        datetime(2024, 1, 1, 0, 0),
        datetime(2024, 4, 1, 0, 0),
        datetime(2024, 5, 1, 0, 0),
        datetime(2024, 6, 1, 0, 0),
        datetime(2024, 8, 1, 0, 0),
    ]
    mock_dict = {
        datetime(2024, 1, 1, 0, 0): {"commits": [1, 2, 3]},
        datetime(2024, 2, 1, 0, 0): {"commits": [4, 5, 6]},
        datetime(2024, 8, 1, 0, 0): {"commits": [12, 1, 6]},
    }
    assert stack_data(x_baseline, mock_dict, "commits") == {
        datetime(2024, 1, 1, 0, 0): [1, 2, 3],
        datetime(2024, 2, 1, 0, 0): [5, 7, 9],
        datetime(2024, 8, 1, 0, 0): [17, 8, 15],
    }
