"""Unitary test for list_tools.py"""

from anubis.list_tools import (
    list_reorder_by_values,
    list_running_avg,
    list_cum_sum,
    list_scale,
)

## ------ TEST_LIST_REORDER_BY_VALUES ------ ##


def test_list_reorder_by_values():
    list_names = ["User1", "User2", "User3", "User4", "User5"]
    list_values = [3, 1, 5, 4, 2]
    result = list_reorder_by_values(list_names, list_values)

    assert result == ["User2", "User5", "User1", "User4", "User3"]


## ------- TEST_LIST_RUNNING_AVG ------ ##


def test_list_running_avg():
    assert list_running_avg([1, 2, 3, 4, 5], 1) == [1.0, 2.0, 3.0, 4.0, 5.0]
    assert list_running_avg([1, 2, 3, 4, 5], 2), [1.5, 2.0, 3.0, 4.0, 4.5]
    assert list_running_avg([1, 2], 5) == [1.5, 1.5]


## ------- TEST_LIST_CUM_SUM ------ ##


def test_list_cum_sum():
    sample_list = [1, 2, 3, 4, 5]
    result = list_cum_sum(sample_list)

    assert result == [1, 3, 6, 10, 15]


## ------ TEST_LIST_SCALE ------ ##


def test_list_scale():
    sample_list = [1, 5, 10, 15, 20]
    scale = 0.5

    result = list_scale(sample_list, scale)
    assert result == [0.5, 2.5, 5, 7.5, 10]
