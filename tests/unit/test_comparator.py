"""Unitary test for comparator.py"""

import pytest

from Levenshtein import jaro

from anubis.comparator import (
    _smart_jaro,
    _compare_two_names,
    _normalize_name,
    _slugify_name,
    _thorough_compare,
    full_compare,
)


## ------ TEST_SMART_JARO ------ ##


def test_smart_jaro():
    name1 = ["j", "e", "a", "n"]
    name2 = ["J", "e", "a", "n"]

    result = _smart_jaro(name1, name2, jaro)
    assert result == True


def test_smart_jaro_different():
    name1 = ["J", "e", "a", "n"]
    name2 = ["P", "h", "i", "l"]

    result = _smart_jaro(name1, name2, jaro)
    assert result == 0.0


def test_smart_jaro_first_different():
    name1 = ["j", "j", "e", "a", "n"]
    name2 = ["j", "e", "a", "n"]

    result = _smart_jaro(name1, name2, jaro)
    assert result == True


def test_smart_jaro_second_different():
    name1 = ["j", "e", "a", "n"]
    name2 = ["j", "j", "e", "a", "n"]

    result = _smart_jaro(name1, name2, jaro)
    assert result == True


def test_smart_jaro_chunk_distance():
    name1 = ["j", "e", "a", "n"]
    name2 = ["t", "h", "e", "o", "d", "u", "l", "e"]

    result = _smart_jaro(name1, name2, jaro)
    assert result == 0.2583333333333333


## ------ TEST_COMPARE_TWO_NAMES ------ ##


def test_compare_two_names_same():
    name1 = "Jean"
    name2 = "Jean"

    assert _compare_two_names(name1, name2) == True


def test_compare_two_names_different():
    name1 = "Jean"
    name2 = "Phil"

    assert _compare_two_names(name1, name2) == False


def test_compare_two_names_close():
    name1 = "Jean"
    name2 = "Dean"

    assert _compare_two_names(name1, name2) == True


## ------ TEST_NORMALIZE_NAME ------ ##


def test_normalize_name():
    name = "Jean ь"
    assert _normalize_name(name) == "Jean "


## ------ TEST_SLUGIFY_NAME ------ ##


def test_slugify_name():
    name = "Jean "
    assert _slugify_name(name) == "Jean"


## ------ TEST_THOROUGH_COMPARE ------ ##


def test_thorough_compare_same():
    name1 = "Jean"
    name2 = "Jean"
    assert _thorough_compare(name1, name2) == True


def test_thorough_compare_different():
    name1 = "Jean"
    name2 = "Phil"
    assert _thorough_compare(name1, name2) == False


# Volontary wrong assertion, this should be false but it's not
def test_thorough_compare_close():
    with pytest.raises(AssertionError):
        name1 = "Jean"
        name2 = "Dean"

        assert _thorough_compare(name1, name2) == False


## ------ TEST_FULL_COMPARE ------ ##


def test_full_compare_same_slugify():
    name1 = "Jean "
    name2 = " Jean"
    assert full_compare(name1, name2) == True


def test_full_compare_long_slug2_start():
    name1 = "Roberto Rastapopoulos"
    name2 = "Roberto Rasta"
    assert full_compare(name1, name2) == True


def test_full_compare_long_slug1_start():
    name1 = "Roberto Rasta"
    name2 = "Roberto Rastapopoulos"
    assert full_compare(name1, name2) == True


def test_full_compare_long_slug2_end():
    name1 = "Roberto Rastapopoulos"
    name2 = "Rastapopoulos"
    assert full_compare(name1, name2) == True


def test_full_compare_long_slug1_end():
    name1 = "Rastapopoulos"
    name2 = "Roberto Rastapopoulos"
    assert full_compare(name1, name2) == True


def test_full_compare_jaro():
    name1 = "Jean Dup"
    name2 = " Jean Du"
    assert full_compare(name1, name2) == True


def test_full_compare_two_names():
    name1 = "Jean jean"
    name2 = "Dean dean"
    assert full_compare(name1, name2) == True
