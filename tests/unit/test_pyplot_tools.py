"""Unitary test for pyplot_tools.py"""

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

from anubis.pyplot_tools import add_refline, save_this_fig


def test_add_refline():
    x = np.linspace(0, 10, 100)
    y = 2 * x + 1
    plt.figure()
    plt.plot(x, y)

    # Add the reference line
    trend_coef = 2.0
    text_ = "Reference Line"
    add_refline(trend_coef, text_)

    # Get the current axes and check the lines and texts
    ax = plt.gca()

    # Check if the reference line is added
    lines = ax.get_lines()
    assert len(lines) == 2

    ref_line = lines[-1]
    xdata = ref_line.get_xdata()
    ydata = ref_line.get_ydata()

    assert np.allclose(ydata, trend_coef * xdata, atol=1e-7)
    assert ref_line.get_linestyle() == ":"
    assert ref_line.get_color() == "black"

    # Check if the reference text is added
    texts = [t for t in ax.texts if t.get_text() == text_]
    assert len(texts) == 1

    ref_text = texts[0]
    assert ref_text.get_position() == (xdata[-1], ydata[-1])
    plt.close()


def test_save_this_fig(tmpdir):
    plt.plot([1, 2, 3], [4, 5, 6])

    fname = "test_plot.svg"
    anubis_out_dir = tmpdir
    save_this_fig(fname, anubis_out_dir)

    saved_file = Path(anubis_out_dir) / fname
    assert saved_file.exists()
