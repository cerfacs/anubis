"""Unitary test for git_helpers.py"""

import pytest

from unittest.mock import Mock

from anubis.git_helpers import (
    parse_git_show_stats,
    _read_diff,
    git_last_revision,
    git_revision_in_between,
    git_revision_stats,
    git_checkout,
    git_blame,
    _blame_file,
    git_branch_status,
    git_size,
    git_tag_history,
)

## ------ GLOBAL FIXTURES TO AVOID USING REAL GIT COMMANDS ------ ##


@pytest.fixture
def mock_subprocess_run_success(mocker):
    mock_result = mocker.Mock()
    mock_result.returncode = 0
    return mocker.patch("anubis.git_helpers.subprocess.run", return_value=mock_result)


@pytest.fixture
def mock_subprocess_run_failure(mocker):
    mock_result = mocker.Mock()
    mock_result.returncode = 1
    return mocker.patch("anubis.git_helpers.subprocess.run", return_value=mock_result)


## ------ TEST PARSE_GIT_SHOW_STATS ------ ##


def test_parse_git_show_stats():
    lines_of_git_show = """
commit 8ca35e331bf225d7e2982c5edbe1e532d139e75f
Author: User 1 <user1@gitmail.com>
Date:   Mon Jun 14 14:20:22 2024 +0000

    My first super commit

 1 file changed, 120 insertion(+), 0 deletion(-)
"""
    revision = "8ca35e331bf225d7e2982c5edbe1e532d139e75f"
    br_type = "master"
    expected_output = {
        "author": "User 1",
        "date": "Mon Jun 14 14:20:22 2024 +0000",
        "files": 1,
        "insertions": 120,
        "deletions": 0,
        "revision": revision,
        "br_type": br_type,
    }

    output = parse_git_show_stats(lines_of_git_show, revision, br_type)
    assert expected_output == output


## ------ TEST _READ_DIFF ------ ##


def test_read_diff():
    context = "2 files changed, 111 insertions(+), 2 deletions(-)"
    expected_files = 2
    expected_insertions = 111
    expected_deletions = 2

    nof_files, nof_insertions, nof_deletions = _read_diff(context)
    assert expected_files == nof_files
    assert expected_insertions == nof_insertions
    assert expected_deletions == nof_deletions


## ------ TEST_GIT_LAST_REVISION ------ ##


def test_git_last_revision_success(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution with a valid revision
    mock_subprocess_run_success.return_value.stdout = b"revision hash\n"

    date = "2024-06-01T12:00:00"
    branch = "master"
    result = git_last_revision(date, branch)

    # Assertion
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "rev-list", "-n 1", "--first-parent", f"--before={date}", branch],
        capture_output=True,
    )
    expected_result = "revision hash"
    assert expected_result == result


def test_git_last_revision_no_revision(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution with a valid revision
    mock_subprocess_run_success.return_value.stdout = b""

    date = "2024-06-01T12:00:00"
    branch = "master"
    result = git_last_revision(date, branch)

    # Assertion
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "rev-list", "-n 1", "--first-parent", f"--before={date}", branch],
        capture_output=True,
    )
    assert result is None


def test_git_last_revision_failure(mock_subprocess_run_failure):
    # Mock subprocess.run for a failed execution
    mock_subprocess_run_failure.return_value.stderr = b"error message"

    # Call git_last_revision with mocked date and branch
    date = "2024-06-01T12:00:00"
    branch = "master"
    result = git_last_revision(date, branch)

    # Assertions
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "rev-list", "-n 1", "--first-parent", f"--before={date}", branch],
        capture_output=True,
    )
    assert result is None


## ------ TEST GIT_REVISION_IN_BETWEEN ------ ##


def test_git_revision_in_between_success(mock_subprocess_run_success):
    before_date = "2023-01-31T00:00:00Z"
    after_date = "2023-01-01T00:00:00Z"
    branch = "main"

    # Mock subprocess.run.side_effect for mock return values
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git rev-list" call with successful output
        Mock(returncode=0, stdout=b"commit1\ncommit2\ncommit3"),
    ]

    # Call the function under test
    result = git_revision_in_between(after_date, before_date, branch)

    # Assert the result matches the expected output
    assert result == ["commit1", "commit2", "commit3"]

    # Ensure the git rev-list command is called once with the correct arguments
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "rev-list", f"--before={before_date}", f"--after={after_date}", branch],
        capture_output=True,
    )


def test_git_revision_in_between_failure(mock_subprocess_run_failure):
    before_date = "2023-01-31T00:00:00Z"
    after_date = "2023-01-01T00:00:00Z"
    branch = "main"

    # Call the function under test
    result = git_revision_in_between(after_date, before_date, branch)

    # Assert the result matches the expected output
    assert result is None

    # Ensure the git rev-list command is called once with the correct arguments
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "rev-list", f"--before={before_date}", f"--after={after_date}", branch],
        capture_output=True,
    )


def test_git_revision_in_between_no_revision(mock_subprocess_run_success):
    before_date = "2023-01-31T00:00:00Z"
    after_date = "2023-01-01T00:00:00Z"
    branch = "main"

    # Mock subprocess.run.side_effect for mock return values
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git rev-list" call with successful output
        Mock(returncode=0, stdout=b""),
    ]

    # Call the function under test
    result = git_revision_in_between(after_date, before_date, branch)

    # Assert the result matches the expected output
    assert result == []


## ------ TEST GIT_REVISION_STATS ------ ##


def test_git_revision_stats_success(mock_subprocess_run_success):
    branch = "main"
    revision_list_main = ["commit1"]
    revision_list_all = ["commit1", "commit2"]

    # Mock subprocess.run for git show
    mock_subprocess_run_success.side_effect = [
        Mock(
            returncode=0,
            stdout=b"""
commit 1
Author: User 1 <user1@gitmail.com>
Date:   Mon Jan 9 14:20:22 2023 +0000

    My first super commit

 1 file changed, 120 insertion(+), 0 deletion(-)
            """,
        ),
        Mock(
            returncode=0,
            stdout=b"""
commit 2
Author: User 2 <user2@gitmail.com>
Date:   Mon Jan 9 14:45:56 2020 +0100

    Second commit

 2 files changed, 58 insertions(+), 604 deletions(-)
             """,
        ),
    ]

    # Call the function under test
    result = git_revision_stats(branch, revision_list_main, revision_list_all)

    # Define the expected result based on mock subprocess output
    expected_result = [
        {
            "author": "User 1",
            "date": "Mon Jan 9 14:20:22 2023 +0000",
            "files": 1,
            "insertions": 120,
            "deletions": 0,
            "revision": "commit1",
            "br_type": "main",
        },
        {
            "author": "User 2",
            "date": "Mon Jan 9 14:45:56 2020 +0100",
            "files": 2,
            "insertions": 58,
            "deletions": 604,
            "revision": "commit2",
            "br_type": "other",
        },
    ]

    # Assert the result matches the expected output
    assert result == expected_result

    # Ensure the git show command is called for each commit in revision_list_all
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "commit1", "--shortstat"], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "commit2", "--shortstat"], capture_output=True
    )


def test_git_revision_stats_git_command_fails(mock_subprocess_run_failure):
    branch = "main"
    revision_list_main = ["commit1"]
    revision_list_all = ["commit1"]

    # Call the function under test
    result = git_revision_stats(branch, revision_list_main, revision_list_all)

    # Assert the result matches the expected output
    assert result == []

    # Ensure the git show command is called for each commit in revision_list_all
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "show", "commit1", "--shortstat"], capture_output=True
    )


def test_git_revision_stats_unicode_error(mock_subprocess_run_success):
    branch = "main"
    revision_list_main = ["commit1"]
    revision_list_all = ["commit1"]

    # Mock subprocess.run to raise a UnicodeDecodeError for one of the git show commands
    mock_subprocess_run_success.side_effect = [
        Mock(
            returncode=0,
            stdout=b"""
commit 1
Author: User 1 <user1@gitmail.com>
Date:   Mon Jan 9 14:20:22 2023 +0000

    My first super commit

 \x80\x81\x82
            """,
        ),
    ]

    # Call the function under test
    result = git_revision_stats(branch, revision_list_main, revision_list_all)

    # Define the expected result based on mock subprocess output
    expected_result = [
        {
            "author": "User 1",
            "date": "Mon Jan 9 14:20:22 2023 +0000",
            "files": 0,
            "insertions": 0,
            "deletions": 0,
            "revision": "commit1",
            "br_type": "main",
        },
    ]

    # Assert the result matches the expected output
    assert result == expected_result


## ------ TEST GIT_CHECKOUT ------ ##


def test_git_checkout_success(mock_subprocess_run_success):
    revision = "main"
    result = git_checkout(revision)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "checkout", "-f", revision], capture_output=True
    )
    assert result is None


def test_git_checkout_failure(mock_subprocess_run_failure):
    revision = "main"
    result = git_checkout(revision)

    # Assertions
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "checkout", "-f", revision], capture_output=True
    )
    assert result is None


## ------ TEST GIT BLAME ------ ##


def test_git_blame_success(mocker, mock_subprocess_run_success):
    # Mock the output of subprocess.run for the 'git ls-files' command
    mock_ls_files_result = mocker.Mock()
    mock_ls_files_result.returncode = 0
    mock_ls_files_result.stdout = b"file1.py\nfile2.py\nfile3.txt\n"

    # Mock the output of subprocess.run for the 'git blame' command
    mock_blame_result = mocker.Mock()
    mock_blame_result.returncode = 0
    mock_blame_result.stdout = (
        b"\t(    author_name\t2023-01-01 08:00:00 +0000\t20)     content"
    )

    # Set the side effect for subprocess.run to return different mocks
    mock_subprocess_run_success.side_effect = [
        mock_ls_files_result,
        mock_blame_result,
        mock_blame_result,
        mock_blame_result,
    ]

    # Define the expected result
    expected_result = [
        {
            "file": "file1.py",
            "author": ["author_name"],
            "date": ["2023-01-01"],
            "indentation": [4],
            "line_number": [20],
        },
        {
            "file": "file2.py",
            "author": ["author_name"],
            "date": ["2023-01-01"],
            "indentation": [4],
            "line_number": [20],
        },
    ]

    # Call the function
    path = "test_path"
    result = git_blame(path)

    # Assertions
    assert mock_subprocess_run_success.call_count == 3
    mock_subprocess_run_success.assert_any_call(
        ["git", "ls-files", path], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "blame", "-c", b"file1.py"], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "blame", "-c", b"file2.py"], capture_output=True
    )
    assert result == expected_result


def test_git_blame_failure(mock_subprocess_run_failure):
    # Mock the output of subprocess.run for a failure case
    mock_subprocess_run_failure.return_value.stdout = b"error message"

    # Call the function
    path = "test_path"
    result = git_blame(path)

    # Assertions
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "ls-files", path], capture_output=True
    )
    assert result is None  # Function should return None in case of failure


## ------ TEST _BLAME_FILE ------ ##


def test_blame_file_success(mock_subprocess_run_success):
    mock_subprocess_run_success.return_value.stdout = (
        b"\t(    author_name\t2023-01-01 08:00:00 +0000\t20)     content"
    )

    # Call _blame_file with a mocked filepath
    filepath = "test_file.py"
    result = _blame_file(filepath)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "blame", "-c", filepath], capture_output=True
    )
    assert result["author"] == ["author_name"]
    assert result["date"] == ["2023-01-01"]
    assert result["indentation"] == [4]
    assert result["line_number"] == [20]


def test_blame_file_failure(mock_subprocess_run_failure):
    mock_subprocess_run_failure.return_value.stderr = b"error message"

    # Call _blame_file with a mocked filepath
    filepath = "test_file.py"
    result = _blame_file(filepath)
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "blame", "-c", filepath], capture_output=True
    )
    # Assertions
    assert result is None


def test_blame_file_no_date(mock_subprocess_run_success):
    # Mock subprocess.run for a case where date is not found
    mock_subprocess_run_success.return_value.stdout = (
        b"\t(    author_name\t20)     content"
    )

    # Call _blame_file with a mocked filepath
    filepath = "test_file.py"
    result = _blame_file(filepath)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "blame", "-c", filepath], capture_output=True
    )
    assert result["author"] == []
    assert result["indentation"] == []
    assert result["line_number"] == []
    assert result["date"] == []


def test_blame_file_no_space(mock_subprocess_run_success):
    # Mock subprocess.run for a case where there is nothing in the line
    mock_subprocess_run_success.return_value.stdout = (
        b"\t(    author_name\t2023-01-01 08:00:00 +0000\t20)            "
    )

    # Call _blame_file with a mocked filepath
    filepath = "test_file.py"
    result = _blame_file(filepath)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "blame", "-c", filepath], capture_output=True
    )
    assert result["author"] == []
    assert result["indentation"] == []
    assert result["line_number"] == []
    assert result["date"] == []


## ------ TEST GIT_BRANCH_STATUS ------ ##


def test_git_branch_status_success(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for multiple calls
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  ref_branch\n  branch1\n  branch2\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch1" call
        Mock(returncode=0, stdout=b"branch1_commit_hash\n"),
        # Mocking the "git rev-list --left-right --count ref_commit...branch1_commit_hash" call
        Mock(returncode=0, stdout=b"3\t4\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch2" call
        Mock(returncode=0, stdout=b"branch2_commit_hash\n"),
        # Mocking the "git rev-list --left-right --count ref_commit...branch2_commit_hash" call
        Mock(returncode=0, stdout=b"5\t6\n"),
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Define the expected result based on mock subprocess outputs
    expected_result = [
        {
            "branch": "branch1",
            "behind": 3,
            "ahead": 4,
            "nb_commits_ref_branch": 10,
        },
        {
            "branch": "branch2",
            "behind": 5,
            "ahead": 6,
            "nb_commits_ref_branch": 10,
        },
    ]

    # Assert the result matches the expected output
    assert result == expected_result

    # Ensure the git branch -a command is called once
    mock_subprocess_run_success.assert_any_call(
        ["git", "branch", "-a"], capture_output=True
    )

    # Ensure the git rev-list --count ref_commit command is called once
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--count", ref_commit], capture_output=True
    )

    # Ensure the git rev-list --first-parent -n 1 --before=date branch1 command is called once
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--first-parent", "-n 1", f"--before={date}", "branch1"],
        capture_output=True,
    )

    # Ensure the git rev-list --first-parent -n 1 --before=date branch2 command is called once
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--first-parent", "-n 1", f"--before={date}", "branch2"],
        capture_output=True,
    )

    # Ensure the git rev-list --left-right --count ref_commit...branch1_commit_hash command is called once
    mock_subprocess_run_success.assert_any_call(
        [
            "git",
            "rev-list",
            "--left-right",
            "--count",
            f"{ref_commit}...branch1_commit_hash",
        ],
        capture_output=True,
    )

    # Ensure the git rev-list --left-right --count ref_commit...branch2_commit_hash command is called once
    mock_subprocess_run_success.assert_any_call(
        [
            "git",
            "rev-list",
            "--left-right",
            "--count",
            f"{ref_commit}...branch2_commit_hash",
        ],
        capture_output=True,
    )


def test_git_branch_status_failure(mock_subprocess_run_failure):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "branch", "-a"], capture_output=True
    )
    assert result is None


def test_git_branch_status_no_branches(mock_subprocess_run_success):
    mock_subprocess_run_success.side_effect = [
        Mock(returncode=0, stdout=b"  ref_branch\n"),  # No other branches
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
    ]

    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"
    result = git_branch_status(ref_commit, date, ref_branch)

    assert result == []
    mock_subprocess_run_success.assert_any_call(
        ["git", "branch", "-a"], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--count", ref_commit], capture_output=True
    )


def test_git_branch_ref_branch_not_found(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for multiple calls
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  branch1\n  branch2\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch1" call
        Mock(returncode=0, stdout=b"branch1_commit_hash\n"),
        # Mocking the "git rev-list --left-right --count ref_commit...branch1_commit_hash" call
        Mock(returncode=0, stdout=b"3\t4\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch2" call
        Mock(returncode=0, stdout=b"branch2_commit_hash\n"),
        # Mocking the "git rev-list --left-right --count ref_commit...branch2_commit_hash" call
        Mock(returncode=0, stdout=b"5\t6\n"),
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Define the expected result based on mock subprocess outputs
    expected_result = [
        {
            "branch": "branch1",
            "behind": 3,
            "ahead": 4,
            "nb_commits_ref_branch": 10,
        },
        {
            "branch": "branch2",
            "behind": 5,
            "ahead": 6,
            "nb_commits_ref_branch": 10,
        },
    ]

    # Assert the result matches the expected output
    assert result == expected_result


def test_git_branch_empty_revision(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for multiple calls
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  ref_branch\n  branch1\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch1" call
        Mock(returncode=0, stdout=b""),
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Assert the result matches the expected output
    assert result == []


def test_git_branch_revision_failed(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for multiple calls
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  ref_branch\n  branch1\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch2" call
        Mock(returncode=1, stderr=b"error\n"),
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Assert the result matches the expected output
    assert result == []


def test_git_branch_status_ref_commit_in_branch_commit(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for mock return values
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  ref_branch\n  branch1\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch1" call
        Mock(
            returncode=0, stdout=b"ref_commit\n"
        ),  # Simulating ref_commit is in branch1
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Assert the result matches the expected output
    assert result == []


def test_git_branch_status_branch_not_processed(mock_subprocess_run_success):
    ref_commit = "ref_commit"
    date = "2023-01-01T00:00:00Z"
    ref_branch = "ref_branch"

    # Mock subprocess.run.side_effect for multiple calls
    mock_subprocess_run_success.side_effect = [
        # Mocking the "git branch -a" call
        Mock(returncode=0, stdout=b"  ref_branch\n  branch1\n"),
        # Mocking the "git rev-list --count ref_commit" call
        Mock(returncode=0, stdout=b"10\n"),
        # Mocking the "git rev-list --first-parent -n 1 --before=date branch1" call
        Mock(returncode=0, stdout=b"branch1_commit_hash\n"),
        # Mocking the "git rev-list --left-right --count ref_commit...branch1_commit_hash" call
        Mock(returncode=1, stderr=b"error_message\n"),
    ]

    # Call the function under test
    result = git_branch_status(ref_commit, date, ref_branch)

    # Assert the result matches the expected output
    assert result == []


## ------ TEST GIT_SIZE ------ ##
@pytest.fixture
def mock_isfile(mocker):
    return mocker.patch("anubis.git_helpers.isfile")


@pytest.fixture
def mock_stat(mocker):
    return mocker.patch("anubis.git_helpers.stat")


def test_git_size_success(mock_subprocess_run_success, mock_isfile, mock_stat):
    # Mock subprocess.run for a successful execution
    mock_subprocess_run_success.return_value.stdout = b"file1.py\nfile2.py\n"

    # Mock os.path.isfile to return True for existing files
    mock_isfile.side_effect = lambda x: x in ["file1.py", "file2.py"]

    # Mock os.stat to return a mock object with st_size attribute
    mock_stat.side_effect = lambda x: Mock(st_size=100 if x == "file1.py" else 200)

    # Call git_size with a mocked path
    path = "test_path"
    result = git_size(path)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "ls-files", path], capture_output=True
    )
    assert result == 300  # 100 (file1.py) + 200 (file2.py)


def test_git_size_failure(mock_subprocess_run_failure):
    # Mock subprocess.run for a failure case
    mock_subprocess_run_failure.return_value.stderr = b"error message"

    # Call git_size with a mocked path
    path = "test_path"
    result = git_size(path)

    # Assertions
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "ls-files", path], capture_output=True
    )
    assert result == {}


def test_git_size_no_files(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution with no files
    mock_subprocess_run_success.return_value.stdout = b""

    # Call git_size with a mocked path
    path = "test_path"
    result = git_size(path)

    # Assertions
    mock_subprocess_run_success.assert_called_once_with(
        ["git", "ls-files", path], capture_output=True
    )
    assert result == 0.0


## ------ TEST GIT_TAG_HISTORY ------ ##


def test_git_tag_history_success(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution with commits and tags
    mock_subprocess_run_success.side_effect = [
        Mock(returncode=0, stdout=b"commit1\ncommit2\ncommit3\n"),
        Mock(returncode=0, stdout=b"tag1\ntag2\n"),
        Mock(returncode=0, stdout=b"commit1 2023-01-01T00:00:00Z\n"),
        Mock(returncode=0, stdout=b"commit2 2023-01-02T00:00:00Z\n"),
    ]

    # Call git_tag_history with mocked ref_branch
    ref_branch = "main"
    result = git_tag_history(ref_branch)

    # Expected result
    expected_result = {
        "tag1": "2023-01-01T00:00:00Z",
        "tag2": "2023-01-02T00:00:00Z",
    }

    # Assertions
    assert result == expected_result
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--first-parent", ref_branch], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(["git", "tag"], capture_output=True)
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "-s", "--no-notes", "--format=%H %aI", "tag1^"],
        capture_output=True,
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "-s", "--no-notes", "--format=%H %aI", "tag2^"],
        capture_output=True,
    )


def test_git_tag_history_no_commits(mock_subprocess_run_failure):
    # Mock subprocess.run for a failed execution for rev-list
    mock_subprocess_run_failure.side_effect = [
        Mock(returncode=1, stderr=b"error message")
    ]

    # Call git_tag_history with mocked ref_branch
    ref_branch = "main"
    result = git_tag_history(ref_branch)

    # Assertions
    assert result == {}
    mock_subprocess_run_failure.assert_called_once_with(
        ["git", "rev-list", "--first-parent", ref_branch], capture_output=True
    )


def test_git_tag_history_no_tags(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution for rev-list but failed for tags
    mock_subprocess_run_success.side_effect = [
        Mock(returncode=0, stdout=b"commit1\ncommit2\ncommit3\n"),
        Mock(returncode=1, stderr=b"error message"),
    ]

    # Call git_tag_history with mocked ref_branch
    ref_branch = "main"
    result = git_tag_history(ref_branch)

    # Assertions
    assert result == {}
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--first-parent", ref_branch], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(["git", "tag"], capture_output=True)


def test_git_tag_history_no_matching_tags(mock_subprocess_run_success):
    # Mock subprocess.run for a successful execution but no tags match the commits
    mock_subprocess_run_success.side_effect = [
        Mock(returncode=0, stdout=b"commit1\ncommit2\ncommit3\n"),
        Mock(returncode=0, stdout=b"tag1\ntag2\n"),
        Mock(returncode=0, stdout=b"other_commit 2023-01-01T00:00:00Z\n"),
        Mock(returncode=0, stdout=b"other_commit 2023-01-02T00:00:00Z\n"),
    ]

    ref_branch = "main"
    result = git_tag_history(ref_branch)

    assert result == {}
    mock_subprocess_run_success.assert_any_call(
        ["git", "rev-list", "--first-parent", ref_branch], capture_output=True
    )
    mock_subprocess_run_success.assert_any_call(["git", "tag"], capture_output=True)
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "-s", "--no-notes", "--format=%H %aI", "tag1^"],
        capture_output=True,
    )
    mock_subprocess_run_success.assert_any_call(
        ["git", "show", "-s", "--no-notes", "--format=%H %aI", "tag2^"],
        capture_output=True,
    )
