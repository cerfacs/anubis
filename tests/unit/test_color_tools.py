"""Unitary tests for color_tools.py"""

from matplotlib.colors import LinearSegmentedColormap, ListedColormap


from anubis.color_tools import get_color, hex_to_rgb, brighten_color


## ------ TEST_GET_COLOR ------ ##


def test_get_color_linear_segmented():
    cmap = LinearSegmentedColormap.from_list("test", [(1, 0, 0), (0, 1, 0), (0, 0, 1)])
    color = get_color(cmap, 1, 3)
    assert color == cmap(int(1 * (cmap.N / 3 + 1)))


def test_get_color_listed():
    cmap = ListedColormap([(1, 0, 0), (0, 1, 0), (0, 0, 1)])
    color = get_color(cmap, 1, 3)
    assert color == cmap(1 % len(cmap.colors))


def test_get_color_list():
    cmap = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]
    color = get_color(cmap, 1, 3)
    assert color == cmap[1 % len(cmap)]


def test_get_color_default():
    cmap = None
    color = get_color(cmap, 1, 3)
    assert color == (1.0, 1.0, 1.0)


## ------ TEST_HEX_TO_RGB ------ ##


def test_hex_to_rgb():
    color1 = "000000"  # Black
    color2 = "FFFFFF"  # White
    color3 = "FF0000"  # Red
    color4 = "00FF00"  # Green
    color5 = "0000FF"  # Blue

    assert hex_to_rgb(color1) == (0, 0, 0)
    assert hex_to_rgb(color2) == (255, 255, 255)
    assert hex_to_rgb(color3) == (255, 0, 0)
    assert hex_to_rgb(color4) == (0, 255, 0)
    assert hex_to_rgb(color5) == (0, 0, 255)


## ------ TEST_BRIGHTEN_COLOR ------ ##


def test_brighten_color():
    # Basic brightening
    original_color = (0.2, 0.4, 0.6, 1.0)
    expected_color = (0.76, 0.82, 0.88, 1.0)
    assert brighten_color(original_color) == expected_color


def test_brighten_color_no_brightenning():
    # Brighter value of 0 (color remains the same)
    original_color = (0.2, 0.4, 0.6, 1.0)
    brighter_value = 0
    expected_color = original_color
    assert brighten_color(original_color, brighter_value) == expected_color


def test_brighten_color_white():
    # Brighter value of 1 (color becomes white)
    original_color = (0.2, 0.4, 0.6, 1.0)
    brighter_value = 1
    expected_color = (1.0, 1.0, 1.0, 1.0)
    assert brighten_color(original_color, brighter_value) == expected_color


def test_brighten_color_negative_value():
    # Negative brighter value (color remains the same)
    original_color = (0.2, 0.4, 0.6, 1.0)
    brighter_value = -0.5
    expected_color = original_color
    assert brighten_color(original_color, brighter_value) == expected_color
