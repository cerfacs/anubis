"""Unitary tests for graph_tools.py"""

from datetime import datetime

import plotly.graph_objects as go

from anubis.graph_tools import get_ranges, add_tags


## ----- TEST_GET_RANGES ------ ##


def test_get_ranges():
    # Create a test figure with 2 traces
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=[datetime(2020, 1, 1), datetime(2020, 1, 2)], y=[1, 3]))
    fig.add_trace(
        go.Scatter(x=[datetime(2019, 12, 31), datetime(2020, 1, 3)], y=[2, 4])
    )

    x_range, y_range = get_ranges(fig)

    expected_x_range = [datetime(2019, 12, 31), datetime(2020, 1, 3)]
    expected_y_range = [0, 4.4]

    assert x_range == expected_x_range
    assert y_range == expected_y_range


## ------ TEST_ADD_TAGS ------ ##


def test_add_tags(datadir):
    x_range = [datetime(2019, 12, 31), datetime(2023, 1, 3)]
    y_range = [0, 10]
    result_annotations, result_lines = add_tags(datadir, x_range, y_range)

    assert result_annotations == [
        {
            "x": datetime(2020, 9, 28),
            "y": 0,
            "text": "v1.0.2",
            "arrowcolor": "rgba(20, 0, 0, 0.2)",
            "arrowside": "none",
            "ax": 0,
            "ayref": "y",
            "ay": 10,
            "borderwidth": 2,
            "bordercolor": "rgba(10, 0, 0, 0.5)",
            "textangle": -45,
        },
        {
            "x": datetime(2020, 12, 21),
            "y": 0,
            "text": "v1.0.3",
            "arrowcolor": "rgba(20, 0, 0, 0.2)",
            "arrowside": "none",
            "ax": 0,
            "ayref": "y",
            "ay": 10,
            "borderwidth": 2,
            "bordercolor": "rgba(10, 0, 0, 0.5)",
            "textangle": -45,
        },
    ]
    assert len(result_lines) == 2
