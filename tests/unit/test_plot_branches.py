"""Unitary tests for plot_branches.py"""

import numpy as np

from anubis.plot_branches import (
    branches_exclude_patterns,
    branches_as_dicts,
    most_delayed_branches,
)


## ------ TEST BRANCHES_EXCLUDE_PATTERNS ------ ##


def test_branches_exclude_patterns():
    branches_per_obs_date = {
        "2022-06": [
            {
                "branch": "feature/one",
                "behind": 10,
                "ahead": 5,
                "nb_commits_ref_branch": 15,
            },
            {
                "branch": "feature/two",
                "behind": 8,
                "ahead": 7,
                "nb_commits_ref_branch": 12,
            },
            {
                "branch": "bugfix/one",
                "behind": 4,
                "ahead": 3,
                "nb_commits_ref_branch": 7,
            },
        ],
        "2022-07": [
            {
                "branch": "feature/three",
                "behind": 6,
                "ahead": 9,
                "nb_commits_ref_branch": 15,
            },
            {
                "branch": "bugfix/two",
                "behind": 2,
                "ahead": 1,
                "nb_commits_ref_branch": 3,
            },
        ],
    }
    exclude_patterns = ["feature"]
    filtered_branches = branches_exclude_patterns(
        branches_per_obs_date, exclude_patterns
    )

    expected_filtered_branches = {
        "2022-06": {
            "branch": np.array(["bugfix/one"]),
            "behind": np.array([4]),
            "ahead": np.array([3]),
            "nb_commits_ref_branch": np.array([7]),
        },
        "2022-07": {
            "branch": np.array(["bugfix/two"]),
            "behind": np.array([2]),
            "ahead": np.array([1]),
            "nb_commits_ref_branch": np.array([3]),
        },
    }

    for date, data in expected_filtered_branches.items():
        assert date in filtered_branches
        assert filtered_branches[date]["branch"] == data["branch"]
        assert filtered_branches[date]["behind"] == data["behind"]
        assert filtered_branches[date]["ahead"] == data["ahead"]
        assert (
            filtered_branches[date]["nb_commits_ref_branch"]
            == data["nb_commits_ref_branch"]
        )


## ------ TEST BRANCHES_AS_DICTS ------ ##


def test_branches_as_dicts():
    filtered_branches = {
        "2022-06": {
            "branch": np.array(["bugfix/one"]),
            "behind": np.array([4]),
            "ahead": np.array([3]),
            "nb_commits_ref_branch": np.array([7]),
        },
        "2022-07": {
            "branch": np.array(["bugfix/two"]),
            "behind": np.array([2]),
            "ahead": np.array([1]),
            "nb_commits_ref_branch": np.array([3]),
        },
    }

    # Expected output
    expected_dict_branches = {
        "bugfix/one": {
            "dates": ["2022-06"],
            "behind": [4],
            "ahead": [3],
            "nb_commits_ref_branch": [7],
        },
        "bugfix/two": {
            "dates": ["2022-07"],
            "behind": [2],
            "ahead": [1],
            "nb_commits_ref_branch": [3],
        },
    }

    # Run the function
    result = branches_as_dicts(filtered_branches)

    for branch, data in expected_dict_branches.items():
        assert branch in result
        assert result[branch]["dates"] == data["dates"]
        assert result[branch]["behind"] == data["behind"]
        assert result[branch]["ahead"] == data["ahead"]
        assert result[branch]["nb_commits_ref_branch"] == data["nb_commits_ref_branch"]


## ------ TEST MOST_DELAYED_BRANCHES ------ ##


def test_most_delayed_branches():
    branches_per_obs_date = {
        "2022-06": {
            "branch": np.array(["feature/one", "feature/two", "bugfix/one"]),
            "behind": np.array([10, 8, 4]),
            "ahead": np.array([5, 7, 3]),
            "nb_commits_ref_branch": np.array([15, 12, 7]),
        },
        "2022-07": {
            "branch": np.array(["feature/three", "bugfix/two"]),
            "behind": np.array([6, 2]),
            "ahead": np.array([9, 1]),
            "nb_commits_ref_branch": np.array([15, 3]),
        },
    }

    # Expected output
    expected_dict_top10 = {
        "feature/one": {
            "dates": ["2022-06"],
            "score": [10],
        },
        "feature/two": {
            "dates": ["2022-06"],
            "score": [8],
        },
        "feature/three": {
            "dates": ["2022-07"],
            "score": [6],
        },
    }

    # Run the function
    result = most_delayed_branches(
        branches_per_obs_date, cat_key="branch", qoi_key="behind", selection_size=3
    )

    # Verify the output
    assert result.keys() == expected_dict_top10.keys()
    for branch, data in expected_dict_top10.items():
        assert branch in result
        assert result[branch]["dates"] == data["dates"]
        assert result[branch]["score"] == data["score"]
