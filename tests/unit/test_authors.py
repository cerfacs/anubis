"""Unitary tests for authors.py"""

from anubis.authors import (
    clean_name,
    find_authors_matches,
    gather_aliases,
    associate_trigram,
    define_trigram,
    _authors_from_blame,
    _authors_from_commits,
    remove_mail_from_author_str,
)


## ------ TEST_CLEAN_NAME ------ ##


def test_clean_name_common_name():
    name1 = "Jean"
    assert clean_name(name1) == ""


def test_clean_name_full_common_name():
    name2 = "Jean Dupont"
    assert clean_name(name2) == "j dupont"


def test_clean_name_with_number():
    name3 = "Karl31"
    assert clean_name(name3) == "karl"


def test_clean_name_with_spe_character():
    name4 = "K@rl"
    assert clean_name(name4) == "k rl"


## ------ TEST_FIND_AUTHORS_MATCHES ------ ##


def test_find_authors_matches():
    authors = [
        "Jean Dupont",
        "Jean Dupont",
        "Karl31",
        "Karl31",
        "Nicolas Dubois",
    ]

    assert find_authors_matches(authors) == {
        "Jean Dupont": 0,
        "Karl31": 0,
        "Nicolas Dubois": 0,
    }


def test_find_authors_matches_twin():
    authors = [
        "Jean Dupont",
        "jean dupont",
        "Karl31",
        "Nicolas Dubois",
    ]

    assert find_authors_matches(authors) == {
        "Jean Dupont": 1,
        "jean dupont": 1,
        "Karl31": 0,
        "Nicolas Dubois": 0,
    }


def test_find_authors_matches_different_names():
    authors = [
        "Karl31",
        "Jean Dupont",
        "Raphael Lain",
        "karl31",
        "Raphael Dupont",
        "Raphaël",
    ]

    result = find_authors_matches(authors)
    assert result == {
        "Karl31": 1,
        "Jean Dupont": 0,
        "Raphael Lain": 2,
        "karl31": 1,
        "Raphael Dupont": 2,
        "Raphaël": 2,
    }


## ------ TEST_GATHER_ALIASES ------ ##


def test_gather_aliases():
    assigned = {
        "Jean Dupont": 0,
        "Karl31": 0,
        "Nicolas Dubois": 0,
    }

    assert gather_aliases(assigned) == {0: []}


def test_gather_aliases_twins():
    assigned = {
        "Jean Dupont": 1,
        "jean dupont": 1,
        "Karl31": 0,
        "Nicolas Dubois": 0,
    }

    assert gather_aliases(assigned) == {
        0: [],
        1: ["Jean Dupont", "jean dupont"],
    }


## ------ TEST_ASSOCIATE_TRIGRAMS ------ ##


def test_associate_trigrams():
    match_dict = {0: ["Jean Dupont"], 1: ["Karl31", "karl31"]}

    assert associate_trigram(match_dict) == {
        "Jean Dupont": "JDT",
        "Karl31": "KAR",
        "karl31": "KAR",
    }


## ------ TEST_DEFINE_TRIGRAM ------ ##


def test_define_trigram():
    name = "Nicolas Dubois"
    assert define_trigram(name) == "NDS"


def test_define_trigram_no_word():
    name = ""
    assert define_trigram(name) == "XXX"


def test_define_trigram_one_name():
    name = "Nicolas"
    assert define_trigram(name) == "NIC"


def test_define_trigram_short_name():
    name = "JC"
    assert define_trigram(name) == "JC"


## ------ TEST_AUTHORS_FROM_BLAME ------ ##


def test_authors_from_blame(datadir):
    result = _authors_from_blame(datadir)
    assert result == [
        "User1",
        "User1",
        "User1",
        "User1",
        "User1",
        "User1",
        "User2",
        "User2",
        "User2",
        "User3",
    ]


## ------ TEST_AUTHORS_FROM_COMMITS ------ ##


def test_authors_from_commits(datadir):
    result = _authors_from_commits(datadir)
    assert result == ["User1", "User2"]


## ------ TEST_REMOVE_MAIL_FROM_AUTHOR_STR ------ ##


def test_remove_mail_from_author_str():
    assert (
        remove_mail_from_author_str("John Smith <john.smith@gitmail.fr>")
        == "John Smith"
    )


def test_remove_mail_from_author_str_fail():
    assert remove_mail_from_author_str(12345) is None
