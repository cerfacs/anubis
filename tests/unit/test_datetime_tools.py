"""Unitary tests for datetime_tools.py"""

from datetime import datetime, timedelta

from anubis.datetime_tools import (
    datetime_to_day,
    datetime_to_month,
    datetime_to_year,
    timedelta_in_months,
    date_merge_semester,
    timedelta_merge,
)

## ------ TEST_DATETIME_TO_DAY ------ ##


def test_datetime_to_day():
    date = datetime(2024, 6, 21, 15, 22, 10)
    assert datetime_to_day(date) == datetime(2024, 6, 21, 0, 0)


## ------- TEST_DATETIME_TO_MONTH ------ ##


def test_datetime_to_month():
    date = datetime(2024, 6, 21, 15, 22, 10)
    assert datetime_to_month(date) == datetime(2024, 6, 1)


## ------- TEST_DATETIME_TO_YEAR ------ ##


def test_datetime_to_year():
    date = datetime(2024, 6, 21, 15, 22, 10)
    assert datetime_to_year(date) == datetime(2024, 1, 1)


## ------ TEST_TIMEDELTA_IN_MONTHS ------ ##


def test_timedelta_in_months():
    date1 = datetime(2024, 6, 21, 15, 22, 10)
    date2 = datetime(2023, 6, 21, 15, 22, 10)
    assert timedelta_in_months(date1, date2) == 12


## ------ TEST DATE_MERGE_SEMESTER ------ ##


def test_date_merge_semester():
    # Test cases
    test_dates = [
        (datetime(2024, 1, 1), "2024-06"),
        (datetime(2024, 6, 30), "2024-06"),
        (datetime(2024, 7, 1), "2024-12"),
        (datetime(2024, 12, 31), "2024-12"),
    ]
    for date_input, expected_output in test_dates:
        result = date_merge_semester(date_input)
        assert result == expected_output


## ------ TEST TIMEDELTA_MERGE ------ ##


def test_timedelta_merge():
    # Test cases
    test_cases = [
        (timedelta(days=30), " 0-3 months"),
        (timedelta(days=120), " 3-6 months"),
        (timedelta(days=210), " 6-9 months"),
        (timedelta(days=300), " 9-12 months"),
        (timedelta(days=400), "12-15 months"),
        (timedelta(days=500), "15-18 months"),
        (timedelta(days=600), "18-21 months"),
        (timedelta(days=700), "21-24 months"),
        (timedelta(days=800), ">2years"),
    ]

    for full_age, expected_output in test_cases:
        result = timedelta_merge(full_age)
        assert result == expected_output
