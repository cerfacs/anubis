"""Unitary test for data_loaders.py"""

import pytest
from datetime import datetime

from anubis.data_loader import (
    anubis_date,
    date_from_pathname,
    load_jsons,
    load_tags,
    load_authors,
)

## ------ TEST_ANUBIS_DATE ------ ##


def test_anubis_date_years_months_format():
    date = "2024-06"
    assert anubis_date(date) == datetime(2024, 6, 1, 0, 0)


def test_anubis_date_years_months_day_format():
    date = "2024-06-21"
    assert anubis_date(date) == datetime(2024, 6, 21, 0, 0)


def test_anubis_date_error():
    with pytest.raises(NotImplementedError):
        wrong_date = "2024WrongDate"
        anubis_date(wrong_date)


## ------ TEST_DATE_FROM_PATHNAME ------ ##


def test_date_from_pathname():
    pathname = "./fake_run_2024-06"
    assert date_from_pathname(pathname) == datetime(2024, 6, 1, 0, 0)


## ------ TEST_LOAD_JSONS ------ ##


def test_load_jsons(datadir):
    fname = "blame.json"
    assert load_jsons(datadir, fname) == {
        datetime(2024, 1, 1, 0, 0): [
            {
                "file": "dummy.py",
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User3",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2020-10-10",
                ],
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
        datetime(2024, 2, 1, 0, 0): [
            {
                "file": "dummy.py",
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User1",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2024-01-10",
                ],
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
    }


def test_load_jsons_start_date_and_early(datadir):
    fname = "blame.json"
    assert load_jsons(datadir, fname, date_start="2024-02") == {
        datetime(2024, 2, 1, 0, 0): [
            {
                "file": "dummy.py",
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User1",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2024-01-10",
                ],
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
    }


def test_load_jsons_end_date_and_late(datadir):
    fname = "blame.json"
    assert load_jsons(datadir, fname, date_end="2024-01") == {
        datetime(2024, 1, 1, 0, 0): [
            {
                "file": "dummy.py",
                "author": [
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User1",
                    "User2",
                    "User2",
                    "User2",
                    "User3",
                ],
                "date": [
                    "2020-09-29",
                    "2020-09-30",
                    "2020-10-04",
                    "2020-10-04",
                    "2020-09-30",
                    "2020-09-30",
                    "2020-10-04",
                    "2021-05-07",
                    "2021-05-05",
                    "2020-10-10",
                ],
                "indentation": [-1, 2, 2, 2, 2, 2, 2, 2, 2, 2],
                "line_number": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            }
        ],
    }


def test_load_jsons_missing_file(datadir):
    fname = "blame.json"
    assert load_jsons(datadir, fname, date_start="2024-03") == {}


def test_load_jsons_fail(datadir):
    with pytest.raises(NotImplementedError):
        fname = "dummy.xml"
        load_jsons(datadir, fname)


## ------ TEST_LOAD_TAGS ------ ##


def test_load_tags(datadir):
    result = load_tags(datadir)
    assert result == {
        "v0.11.2": datetime(2020, 9, 28),
        "v0.11.3": datetime(2020, 12, 21),
    }


## ------ TEST_LOAD_AUTHORS ------ ##


def test_load_authors(datadir):
    result = load_authors(datadir)
    assert result == {
        "karl31": "KAR",
        "jean dupont": "JDT",
        "raphael lain": "RLN",
        "raphael dupont": "RLT",
        "raphaël": "RLN",
    }
