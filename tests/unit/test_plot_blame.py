"""Unitary tests for plot_blame.py"""

from datetime import datetime

from anubis.plot_blame import (
    gather_by_age,
    gather_by_author,
)


## ------ TEST GATHER_BY_AGE ------ ##

test_data = {
    datetime(2022, 10, 1): [
        {
            "file": "tools.py",
            "author": [
                "Theophile Rondinaud",
                "Theophile Rondinaud",
                "Theophile Rondinaud",
            ],
            "date": ["2022-02-10", "2022-02-10", "2022-02-10"],
            "indentation": [-1, -1, 1],
            "line_number": [1, 2, 3],
        },
        {
            "file": "utils.py",
            "author": [
                "Theodule Brincan",
                "Theodule Brincan",
                "Theodule Brincan",
            ],
            "date": ["2021-10-01", "2021-10-01", "2021-10-01"],
            "indentation": [1, 1, 1],
            "line_number": [1, 2, 3],
        },
    ],
    datetime(2022, 11, 1): [
        {
            "file": "tools.py",
            "author": [
                "Theophile Rondinaud",
                "Theophile Rondinaud",
                "Theophile Rondinaud",
            ],
            "date": ["2022-02-10", "2022-02-10", "2022-02-10"],
            "indentation": [-1, -1, 1],
            "line_number": [1, 2, 3],
        },
        {
            "file": "utils.py",
            "author": [
                "Norbert Colliou",
                "Norbert Colliou",
                "Norbert Colliou",
            ],
            "date": ["2022-10-21", "2022-10-21", "2021-10-21"],
            "indentation": [1, 1, 1],
            "line_number": [1, 2, 3],
        },
    ],
}


def test_gather_by_age_true():
    expected_result = {
        datetime(2022, 10, 1, 0, 0): {" 6-9 months": 3, "12-15 months": 3},
        datetime(2022, 11, 1, 0, 0): {
            " 0-3 months": 2,
            " 6-9 months": 3,
            "12-15 months": 1,
        },
    }

    result = gather_by_age(test_data)
    assert expected_result == result


def test_gather_by_age_false():
    expected_result = {
        datetime(2022, 10, 1, 0, 0): {"2021-12": 3, "2022-06": 3},
        datetime(2022, 11, 1, 0, 0): {
            "2021-12": 1,
            "2022-06": 3,
            "2022-12": 2,
        },
    }

    result = gather_by_age(test_data, by_age=False)
    assert expected_result == result


## ------ TEST GATHER_BY_AUTHOR ------ ##


def test_gather_by_author():
    authors = {
        "theophile rondinaud": "TRD",
        "theodule brincan": "TBN",
    }
    expected_output = {
        datetime(2022, 10, 1): {
            "TRD": 3,
            "TBN": 3,
        },
        datetime(2022, 11, 1): {
            "TRD": 3,
            "XXX": 3,
        },
    }

    result = gather_by_author(test_data, authors)

    assert result == expected_output
