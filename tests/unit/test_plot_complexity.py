"""Unitary tests for plot_complexity.py"""

import numpy as np


from anubis.plot_complexity import (
    normalize_score,
    complexity_score,
    complexity_exclude_patterns,
    complexity_worst_performers,
    _create_complexity_plot_database,
    create_worst_performers_database,
)


## ------ TEST NORMALIZE_SCORE ------ ##


def test_normalize_score():
    in_ = np.array([1, 10, 20, 30, 40, 50])
    val_for_0 = 10
    factor_for_10 = 100
    assert np.allclose(
        normalize_score(in_, val_for_0, factor_for_10=factor_for_10),
        np.array([0, 0, 1.5, 2.38, 3.01, 3.49]),
        atol=0.01,
    )


def test_normalize_score_below_threshold():
    in_ = np.array([0, 5, 8, 9])
    val_for_0 = 10
    assert (normalize_score(in_, val_for_0) == np.array([0, 0, 0, 0])).all()


def test_normalize_score_same_value_as_val_for0():
    in_ = np.array([10])
    val_for_0 = 10
    factor_for_10 = 60
    assert normalize_score(in_, val_for_0, factor_for_10) == np.array([0])


## ------ TEST COMPLEXITY_SCORE ------ ##


def test_complexity_score():
    # Sample input data
    joined_complexity = {
        "2023-01-01": {
            "CCN": [12, 15, 20],
            "IDT": [60, 80, 100],
            "NLOC": [100, 150, 200],
            "param": [6, 8, 10],
        },
        "2023-02-01": {
            "CCN": [10, 13, 25],
            "IDT": [50, 70, 90],
            "NLOC": [120, 170, 220],
            "param": [5, 7, 9],
        },
    }

    # Expected output (simplified for the sake of demonstration)
    expected_output = {
        "2023-01-01": {
            "score": (
                normalize_score(np.array([12, 15, 20]), 10.0)
                + normalize_score(np.array([60, 80, 100]), 50.0)
                + normalize_score(np.array([100, 150, 200]), 50.0)
                + normalize_score(np.array([6, 8, 10]), 5.0)
            )
            / 4.0,
            "cyclomatic": normalize_score(np.array([12, 15, 20]), 10.0),
            "indentation": normalize_score(np.array([60, 80, 100]), 50.0),
            "params": normalize_score(np.array([6, 8, 10]), 5.0),
            "size": normalize_score(np.array([100, 150, 200]), 50.0),
        },
        "2023-02-01": {
            "score": (
                normalize_score(np.array([10, 13, 25]), 10.0)
                + normalize_score(np.array([50, 70, 90]), 50.0)
                + normalize_score(np.array([120, 170, 220]), 50.0)
                + normalize_score(np.array([5, 7, 9]), 5.0)
            )
            / 4.0,
            "cyclomatic": normalize_score(np.array([10, 13, 25]), 10.0),
            "indentation": normalize_score(np.array([50, 70, 90]), 50.0),
            "params": normalize_score(np.array([5, 7, 9]), 5.0),
            "size": normalize_score(np.array([120, 170, 220]), 50.0),
        },
    }

    result = complexity_score(joined_complexity)

    # Compare the result with the expected output
    for date in expected_output:
        for key in expected_output[date]:
            assert np.allclose(result[date][key], expected_output[date][key], atol=0.01)


## ------ TEST COMPLEXITY_EXCLUDE_PATTERNS ------ ##


def test_complexity_exclude_patterns():
    # Sample input data
    joined_complexity = {
        "2023-01-01": {
            "file": ["main.py", "test_main.py", "utils.py", "exclude_this.py"],
            "CCN": [10, 20, 30, 40],
            "IDT": [50, 60, 70, 80],
            "NLOC": [100, 200, 300, 400],
            "param": [5, 10, 15, 20],
        },
        "2023-02-01": {
            "file": ["main2.py", "test_main2.py", "utils2.py", "exclude_this2.py"],
            "CCN": [11, 21, 31, 41],
            "IDT": [51, 61, 71, 81],
            "NLOC": [101, 201, 301, 401],
            "param": [6, 11, 16, 21],
        },
    }

    # Exclusion patterns
    exclude_patterns = ["test_", "exclude_this", "non_existant"]

    # Expected output
    expected_output = {
        "2023-01-01": {
            "file": ["main.py", "utils.py"],
            "CCN": [10, 30],
            "IDT": [50, 70],
            "NLOC": [100, 300],
            "param": [5, 15],
        },
        "2023-02-01": {
            "file": ["main2.py", "utils2.py"],
            "CCN": [11, 31],
            "IDT": [51, 71],
            "NLOC": [101, 301],
            "param": [6, 16],
        },
    }

    # Run the function with the sample input
    result = complexity_exclude_patterns(joined_complexity, exclude_patterns)

    # Compare the result with the expected output
    assert set(result.keys()) == set(expected_output.keys())
    for date in result:
        assert (result[date]["file"] == expected_output[date]["file"]).all()
        assert (result[date]["CCN"] == expected_output[date]["CCN"]).all()
        assert (result[date]["IDT"] == expected_output[date]["IDT"]).all()
        assert (result[date]["NLOC"] == expected_output[date]["NLOC"]).all()
        assert (result[date]["param"] == expected_output[date]["param"]).all()


def test_exclude_pattern_empty_file():
    # Sample input data
    joined_complexity = {
        "2023-01-01": {
            "file": [],
            "CCN": [],
            "IDT": [],
            "NLOC": [],
            "param": [],
        },
    }
    result = complexity_exclude_patterns(joined_complexity)
    assert result == {}


def test_exclude_pattern_exception_triggered():
    joined_complexity = {
        "2023-01-01": {
            "file": ["main.py", "utils.py"],
            "CCN": [10, 30],
            "IDT": [50, 70],
            "NLOC": [100, 300],
            "param": [5, 15],
        },
    }

    date = "2023-01-01"
    result = complexity_exclude_patterns(
        joined_complexity, exclude_patterns=["main", "utils"]
    )
    assert result == {}


## ------ TEST COMPLEITY_WORST_PERFORMERS ------ ##


def test_complexity_worst_performers():
    joined_complexity = {
        "2023-01-01": {
            "file": ["main.py", "utils.py", "test_main.py"],
            "function": ["func1", "func2", "func3"],
            "CCN": [15, 30, 25],
            "IDT": [100, 200, 150],
            "NLOC": [300, 400, 350],
            "param": [5, 10, 7],
        },
        "2023-02-01": {
            "file": ["main.py", "utils.py", "test_main.py"],
            "function": ["func1", "func2", "func3"],
            "CCN": [10, 20, 35],
            "IDT": [90, 180, 160],
            "NLOC": [280, 420, 360],
            "param": [4, 11, 8],
        },
    }

    # Expected complexity scores (based on the logic in complexity_score)
    computed_scores = {
        "2023-01-01": {
            "score": (
                normalize_score(np.array([15, 30, 25]), 10.0)
                + normalize_score(np.array([100, 200, 150]), 50.0)
                + normalize_score(np.array([300, 400, 350]), 50.0)
                + normalize_score(np.array([5, 10, 7]), 5.0)
            )
            / 4.0
        },
        "2023-02-01": {
            "score": (
                normalize_score(np.array([10, 20, 35]), 10.0)
                + normalize_score(np.array([90, 180, 160]), 50.0)
                + normalize_score(np.array([280, 420, 360]), 50.0)
                + normalize_score(np.array([4, 11, 8]), 5.0)
            )
            / 4.0
        },
    }

    # Compute worst performers
    result = complexity_worst_performers(joined_complexity, nfunc=2, ctype="score")

    expected_keys = {"utils.py/\nfunc2", "test_main.py/\nfunc3"}
    assert set(result.keys()) == expected_keys

    expected_scores = {
        "utils.py/\nfunc2": np.array(
            [
                computed_scores["2023-01-01"]["score"][1],
                computed_scores["2023-02-01"]["score"][1],
            ]
        ),
        "test_main.py/\nfunc3": np.array(
            [
                computed_scores["2023-01-01"]["score"][2],
                computed_scores["2023-02-01"]["score"][2],
            ]
        ),
    }

    # Verify the dates and scores for each worst performer
    for name, data in result.items():
        assert len(data["dates"]) == 2  # Both dates should be present
        assert data["dates"] == ["2023-01-01", "2023-02-01"]
        assert np.allclose(data["score"], expected_scores[name], atol=1e-5)


## ------ TEST _CREATE_COMPLEXITY_PLOT_DATABASE ------ ##


def test_create_complexity_plot_database():
    joined_complexity = {
        "2023-01-01": {
            "file": ["main.py", "utils.py", "test_main.py"],
            "function": ["func1", "func2", "func3"],
            "CCN": [15, 30, 25],
            "IDT": [100, 200, 150],
            "NLOC": [300, 400, 350],
            "param": [5, 10, 7],
        },
        "2023-02-01": {
            "file": ["main.py", "utils.py", "test_main.py"],
            "function": ["func1", "func2", "func3"],
            "CCN": [10, 20, 35],
            "IDT": [90, 180, 160],
            "NLOC": [280, 420, 360],
            "param": [4, 11, 8],
        },
    }

    result = _create_complexity_plot_database(joined_complexity)

    expected_dict = {
        "cyclomatic": {
            "dates": ["2023-01-01", "2023-02-01"],
            "values": [2.0511131197, 1.7099437538],
        },
        "indentation": {
            "dates": ["2023-01-01", "2023-02-01"],
            "values": [2.667968630, 2.5836530414],
        },
        "params": {
            "dates": ["2023-01-01", "2023-02-01"],
            "values": [0.9188613104, 1.1528866226],
        },
        "score": {
            "dates": ["2023-01-01", "2023-02-01"],
            "values": [2.6018232503, 2.563751393],
        },
        "size": {
            "dates": ["2023-01-01", "2023-02-01"],
            "values": [4.76934994, 4.808522155],
        },
    }

    for key in expected_dict:
        assert result[key]["dates"] == expected_dict[key]["dates"]
        assert np.allclose(
            result[key]["values"], expected_dict[key]["values"], atol=1e-5
        )


## ------ TEST CREATE_WORST_PERFORMERS_DATABASE ------ ##
def test_create_worst_performers_database():
    dict_top10 = {
        "test_main.py/\nfunc3": {
            "dates": ["2023-01-01", "2023-02-01"],
            "score": np.array([2.62391534, 2.96750698]),
        },
        "utils.py/\nfunc2": {
            "dates": ["2023-01-01", "2023-02-01"],
            "score": np.array([3.21021796, 2.98629611]),
        },
    }

    result = create_worst_performers_database(dict_top10, n_element=1)

    for name, data in result.items():
        assert name == "utils.py/\nfunc2"
        assert data["dates"] == ["2023-01-01", "2023-02-01"]
        assert np.allclose(data["score"], np.array([3.21021796, 2.98629611]), atol=1e-5)
