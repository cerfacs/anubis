"""Unitary tests for timegraph.py"""

import pytest
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.dates import date2num
from matplotlib.colors import ListedColormap
from datetime import datetime, timedelta

from tol_colors import tol_cset

from anubis.timegraph import axis_plainticks, AnubisTimeGraph


## ------ TEST AXIS_PLAINTICKS ------ ##


@pytest.mark.parametrize(
    "axis, set_scale, set_lim, get_lim, get_ticks, get_labels",
    [
        (
            "y",
            lambda ax: ax.set_yscale("log"),
            lambda ax: ax.set_ylim(1, 1000),
            lambda ax: ax.get_ylim(),
            lambda ax: ax.yaxis.get_majorticklocs(),
            lambda ax: [label.get_text() for label in ax.get_yticklabels()],
        ),
        (
            "x",
            lambda ax: ax.set_xscale("log"),
            lambda ax: ax.set_xlim(1, 1000),
            lambda ax: ax.get_xlim(),
            lambda ax: ax.xaxis.get_majorticklocs(),
            lambda ax: [label.get_text() for label in ax.get_xticklabels()],
        ),
    ],
)
def test_axis_plainticks(axis, set_scale, set_lim, get_lim, get_ticks, get_labels):
    fig, ax = plt.subplots()

    # Setting axis to logarithmic scale for the test
    set_scale(ax)
    set_lim(ax)

    axis_plainticks(ax, axis=axis)

    # Check that the axis limits are set correctly
    assert get_lim(ax) == (1, 1000)

    # Check that the major ticks are set correctly
    expected_ticks = [1, 10, 100, 1000]
    actual_ticks = get_ticks(ax)
    assert list(actual_ticks) == expected_ticks

    # Check that the major tick labels are set correctly
    expected_labels = ["1", "10", "100", "1000"]
    actual_labels = get_labels(ax)
    assert actual_labels == expected_labels

    plt.close(fig)


## ------ TEST ANUBIS_TIMEGRAPH ------ ##


@pytest.mark.parametrize(
    "title, xlabel",
    [
        ("Test Title", "Test X-Label"),
        (None, "Date"),
    ],
)
def test_anubis_timegraph(title, xlabel):
    graph = AnubisTimeGraph(title=title, xlabel=xlabel)

    # Check if the figure and axes are created
    assert isinstance(graph.fig, plt.Figure)
    assert isinstance(graph.ax, plt.Axes)

    # Check if the title is set correctly
    if title is not None:
        assert graph.ax.get_title() == title
    else:
        assert graph.ax.get_title() == ""

    # Check if the xlabel is set correctly
    assert graph.ax.get_xlabel() == xlabel

    # Check tick parameters for x-axis
    x_tick_params = graph.ax.xaxis.get_tick_params()
    assert x_tick_params["labelsize"] == 17
    assert x_tick_params["length"] == 10
    assert x_tick_params["width"] == 3

    # Check tick parameters for y-axis
    y_tick_params = graph.ax.yaxis.get_tick_params()
    assert y_tick_params["labelsize"] == 17
    assert y_tick_params["length"] == 10
    assert y_tick_params["width"] == 2

    # Check if despine is applied
    assert not graph.ax.spines["top"].get_visible()
    assert not graph.ax.spines["right"].get_visible()

    # Check if change_cset is called and sets the cset
    expected_cset = tol_cset("muted")
    assert graph.cset == expected_cset

    plt.close(graph.fig)


## ------ TEST ANUBIS_TIMEGRAPH.CSET_LEN ------ ##


@pytest.mark.parametrize(
    "cset_name, expected_len",
    [
        ("muted", len(tol_cset("muted"))),
        ("vibrant", len(tol_cset("vibrant"))),
    ],
)
def test_cset_len(cset_name, expected_len):
    graph = AnubisTimeGraph()
    graph.change_cset(cset_name)

    assert graph.cset_len() == expected_len


## ------ TEST ANUBIS_TIMEGRAPH.COLOR_IDX ------ ##


@pytest.mark.parametrize(
    "cset_name, idx, expected_color",
    [
        ("muted", 0, list(tol_cset("muted"))[0]),
        ("muted", 100, "black"),  # Out of bounds
    ],
)
def test_color_idx(cset_name, idx, expected_color):
    graph = AnubisTimeGraph()
    graph.change_cset(cset_name)
    color = graph.color_idx(idx)

    assert color == expected_color


## ------ TEST ANUBIS_TIMEGRAPH.ENLARGE_RIGHT ------ ##


def test_enlarge_right():
    graph = AnubisTimeGraph()
    graph.ax.set_xlim(0, 10)
    graph.enlarge_right(factor=1.2)
    min_x, max_x = graph.ax.get_xlim()

    assert min_x == 0
    assert max_x == 12.0


## ------ TEST ANUBIS_TIMEGRAPH.ENLARGE_BOTTOM ------ ##


def test_enlarge_bottom():
    graph = AnubisTimeGraph()
    graph.ax.set_ylim(0, 10)
    graph.enlarge_bottom(factor=0.2)
    min_y, max_y = graph.ax.get_ylim()

    assert min_y == -2.0
    assert max_y == 10


## ------ TEST ANUBIS_TIMEGRAPH.ADD_TAGS ------ ##


def test_add_tags():
    graph = AnubisTimeGraph()

    # Set up predefined x and y limits
    graph.ax.set_xlim(date2num(datetime(2023, 1, 1)), date2num(datetime(2023, 12, 31)))
    graph.ax.set_ylim(0, 10)

    # Define the tag dictionary
    tag_dict = {
        "v1.0": datetime(2023, 3, 15),
        "v1.1": datetime(2023, 6, 30),
        "v2.0": datetime(2024, 1, 1),  # This tag is out of the x-axis range
    }

    # Add tags to the graph
    graph.add_tags(tag_dict)

    # Retrieve the text annotations
    texts = graph.ax.texts
    assert len(texts) == 2

    # Verify the tags are correctly placed
    expected_tags = {
        "v1.0": datetime(2023, 3, 15),
        "v1.1": datetime(2023, 6, 30),
    }
    for text in texts:
        assert text.get_text() in expected_tags
        assert text.get_position()[0] == expected_tags[text.get_text()]
        assert text.get_position()[1] == graph.ax.get_ylim()[1]
        assert text.get_ha() == "left"
        assert text.get_va() == "top"
        assert text.get_rotation() == 45


## ------ TEST ANUBIS_TIMEGRAPH.EOL_VALUES ------ ##


def test_eol_values():
    graph = AnubisTimeGraph()

    # Create some lines on the plot
    dates = [
        date2num(datetime(2023, 1, 1)),
        date2num(datetime(2023, 2, 1)),
        date2num(datetime(2023, 3, 1)),
    ]
    values1 = [100, 150, 200]
    values2 = [50, 75, 125]
    graph.ax.plot(dates, values1, label="Line 1")
    graph.ax.plot(dates, values2, label="Line 2")

    # Call the eol_values method
    graph.eol_values(datetime(2023, 4, 1))

    # Check if the dotted lines and text annotations are added correctly
    assert len(graph.ax.lines) == 4  # 2 original lines + 2 dotted lines
    assert len(graph.ax.texts) == 2  # 2 text annotations

    # Verify the positions and content of the text annotations
    expected_y = [200, 125]
    for idx, text in enumerate(graph.ax.texts):
        assert "{:.2f}".format(expected_y[idx]) in text.get_text()
        assert text.get_color() == graph.ax.lines[idx].get_color()
        assert text.get_position()[1] == pytest.approx(
            expected_y[idx] * 0.993, rel=1e-2
        )


def test_eol_values_overlapping():
    graph = AnubisTimeGraph()

    # Create some lines on the plot
    dates = [
        date2num(datetime(2023, 1, 1)),
        date2num(datetime(2023, 2, 1)),
        date2num(datetime(2023, 3, 1)),
    ]
    values1 = [100, 150, 100]
    values2 = [50, 75, 125]
    graph.ax.plot(dates, values1, label="Line 1")
    graph.ax.plot(dates, values2, label="Line 2")

    # Call the eol_values method
    graph.eol_values(dates[-1])

    # Verify the positions and content of the text annotations
    expected_y = [100, 97.55]
    for idx, text in enumerate(graph.ax.texts):
        expected_y_text = values1[-1] if idx == 0 else values2[-1]
        assert "{:.2f}".format(expected_y_text) in text.get_text()
        assert text.get_color() == graph.ax.lines[idx].get_color()
        assert text.get_position()[1] == pytest.approx(
            expected_y[idx] * 0.993, rel=1e-2
        )


def test_eol_values_without_dates():
    graph = AnubisTimeGraph()

    # Create some lines on the plot
    x_values = [1, 2, 3]
    values1 = [100, 150, 200]
    values2 = [50, 75, 125]
    graph.ax.plot(x_values, values1, label="Line 1")
    graph.ax.plot(x_values, values2, label="Line 2")

    # Call the eol_values method
    values = ["val_1", "val_2"]
    graph.eol_values(x_values[-1], values=values)

    # Check if the dotted lines and text annotations are added correctly
    assert len(graph.ax.lines) == 4  # 2 original lines + 2 dotted lines
    assert len(graph.ax.texts) == 2  # 2 text annotations

    # Verify the positions and content of the text annotations
    expected_y = [200, 125]
    for idx, text in enumerate(graph.ax.texts):
        assert values[idx] in text.get_text()
        assert text.get_color() == graph.ax.lines[idx].get_color()

        # Check y position of the text
        assert text.get_position()[1] == pytest.approx(
            expected_y[idx] * 0.993, rel=1e-2
        )


## ------ TEST ANUBIS_TIMEGRAPH.ADD_BRACKETS_VAL ------ ##


def test_add_brackets_val():
    graph = AnubisTimeGraph()

    # Create some lines on the plot with datetime x-axis values
    dates = [
        date2num(datetime(2023, 1, 1)),
        date2num(datetime(2023, 2, 1)),
        date2num(datetime(2023, 3, 1)),
    ]
    values1 = [100, 150, 200]
    values2 = [150, 225, 325]
    graph.ax.plot(dates, values1, label="Line 1")
    graph.ax.plot(dates, values2, label="Line 2")

    # Define stack_data and worst_candidates
    stack_data = {"key1": [100, 150, 200], "key2": [150, 225, 325]}
    worst_candidates = {"key1": "Candidate 1", "key2": "Candidate 2"}

    # Call the add_brackets_val method
    graph.add_brackets_val(stack_data, worst_candidates, display_key=True)

    # Check if the brackets and text annotations are added correctly
    # We expect 2 additional plot calls for the brackets
    assert len(graph.ax.lines) == 4  # 2 original lines + 2 bracket lines
    assert len(graph.ax.texts) == 2  # 2 text annotations

    # Verify the positions and content of the text annotations
    expected_text = ["key1", "key2"]
    for idx, text in enumerate(graph.ax.texts):
        assert text.get_text() == expected_text[idx]

        x_text = text.get_position()[0]
        y_text = text.get_position()[1]

        # Check x position of the text
        expected_x_text = datetime(2023, 3, 1) + timedelta(days=20)
        assert x_text == expected_x_text

        # Check y position of the text
        expected_y_text_pos = [99.0, 259.875]
        assert y_text == pytest.approx(expected_y_text_pos[idx], rel=1e-2)

    # Verify the positions of the bracket lines
    bracket_lines = graph.ax.lines[2:]
    for idx, line in enumerate(bracket_lines):
        xdata = line.get_xdata()
        ydata = line.get_ydata()

        x_ref = datetime.utcfromtimestamp(
            int(graph.ax.lines[0].get_xdata()[-1]) * 86400
        )

        expected_xcoord_bracket = [
            x_ref + timedelta(days=4),
            x_ref + timedelta(days=8),
            x_ref + timedelta(days=8),
            x_ref + timedelta(days=4),
        ]
        assert xdata == pytest.approx(expected_xcoord_bracket, rel=1e-2)

        expected_ycoord_bracket = [[200, 200, 0, 0], [325, 325, 200, 200]]
        assert ydata == pytest.approx(expected_ycoord_bracket[idx], rel=1e-2)


## ------ TEST ANUBIS_TIMEGRAPH.CREATE_LINEPLOT ------ ##


def test_create_lineplot():
    graph = AnubisTimeGraph()

    data_dict = {
        "Candidate 1": {
            "date": [datetime(2023, 1, 1), datetime(2023, 2, 1), datetime(2023, 3, 1)],
            "value": [100, 150, 200],
        },
        "Candidate 2": {
            "date": [datetime(2023, 1, 1), datetime(2023, 2, 1), datetime(2023, 3, 1)],
            "value": [50, 75, 125],
        },
    }
    cmap = ListedColormap(["red", "blue"])

    # Call the create_lineplot method
    graph.create_lineplot(
        data_dict=data_dict,
        key_for_x="date",
        key_for_y="value",
        cmap=cmap,
        drawstyle="default",
        marker="o",
    )

    # Check if the lines are plotted correctly
    assert len(graph.ax.lines) == 2  # We have two candidates

    line1, line2 = graph.ax.lines
    # Check the data of the first line
    assert list(line1.get_xdata()) == [
        date2num(datetime(2023, 1, 1)),
        date2num(datetime(2023, 2, 1)),
        date2num(datetime(2023, 3, 1)),
    ]
    assert list(line1.get_ydata()) == [100, 150, 200]
    assert line1.get_color() == (1.0, 0, 0, 1.0)  # Red
    assert line1.get_marker() == "o"

    # Check the data of the second line
    assert list(line2.get_xdata()) == [
        date2num(datetime(2023, 1, 1)),
        date2num(datetime(2023, 2, 1)),
        date2num(datetime(2023, 3, 1)),
    ]
    assert list(line2.get_ydata()) == [50, 75, 125]
    assert line2.get_color() == (0, 0, 1.0, 1.0)  # Blue
    assert line2.get_marker() == "o"

    # Check if the legend is created correctly
    handles, labels = graph.ax.get_legend_handles_labels()
    assert len(handles) == 2  # Two legend entries
    assert labels == ["Candidate 1", "Candidate 2"]

    # Check if the bottom spine is not visible
    assert not graph.ax.spines["bottom"].get_visible()


## ------ TEST ANUBIS_TIMEGRAPH.CREATE_STACKPLOT ------ ##


@pytest.mark.parametrize(
    "key_to_color, colors",
    [
        (
            "Region 2",
            [
                (0.0, 0.0, 0.0),  # Line
                (0.3, 0.3, 0.3),  # Area
                (0.0, 0.0, 0.0),
                (0.0, 0.0, 1.0),
                (0.0, 0.0, 0.0),
                (0.3, 0.3, 0.3),
                (0.0, 0.0, 0.0),
            ],
        ),
        (
            None,
            [
                (0.0, 0.0, 0.0),  # Line
                (1.0, 0.0, 0.0),  # Area
                (0.0, 0.0, 0.0),
                (0.0, 0.0, 1.0),
                (0.0, 0.0, 0.0),
                (0.0, 0.5, 0.0),
                (0.0, 0.0, 0.0),
            ],
        ),
    ],
)
def test_create_stackplot(key_to_color, colors):
    graph = AnubisTimeGraph()

    x_baseline = [1, 2, 3, 4, 5]
    stack_data = {
        "Region 1": [1, 2, 3, 2, 1],
        "Region 2": [2, 3, 4, 3, 2],
        "Region 3": [3, 4, 5, 4, 3],
    }
    cmap = ListedColormap(["red", "blue", "green"])

    # Call the create_stackplot method
    graph.create_stackplot(
        x_baseline=x_baseline,
        stack_data=stack_data,
        key_to_color=key_to_color,
        cmap=cmap,
        show_legend=True,
    )

    # Check if the lines and filled areas are plotted correctly
    assert (
        len(graph.ax.lines) == 3
    )  # We should have as many lines as there are keys in stack_data

    # Verify the properties of the plotted lines
    for idx, line in enumerate(graph.ax.lines):
        assert list(line.get_ydata()) == list(stack_data.values())[idx]
        assert line.get_color() == "#000000"  # Lines are plotted in black

    # Verify the properties of the filled areas
    for idx, poly in enumerate(graph.ax.collections):
        facecolor = poly.get_facecolor()[0][:3]  # Extract the RGB values
        assert np.allclose(
            facecolor, colors[idx], atol=1e-2
        )  # Check the face color of the filled areas

    # Check if the legend is created correctly
    handles, labels = graph.ax.get_legend_handles_labels()
    assert len(handles) == 3
    assert labels == ["Region 1", "Region 2", "Region 3"]

    # Verify the position of the legend
    legend = graph.ax.get_legend()
    assert legend is not None
    assert legend.get_bbox_to_anchor()._bbox.x0 == 0.5
    assert legend.get_bbox_to_anchor()._bbox.y0 == -0.2

    # Check if the bottom spine is not visible
    assert not graph.ax.spines["bottom"].get_visible()
