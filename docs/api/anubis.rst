anubis package
==============

.. automodule:: anubis
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

anubis.external\_analysis module
--------------------------------

.. automodule:: anubis.external_analysis
   :members:
   :undoc-members:
   :show-inheritance:

anubis.git\_helpers module
--------------------------

.. automodule:: anubis.git_helpers
   :members:
   :undoc-members:
   :show-inheritance:

anubis.timemachine module
-------------------------

.. automodule:: anubis.timemachine
   :members:
   :undoc-members:
   :show-inheritance:
