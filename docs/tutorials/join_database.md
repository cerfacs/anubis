
### Join database


Once you have generated the database with the [previous tutorial](generate_database.md), you may want to concatenate the generated database.
Let us have a look on a previously generated database.

```bash
ANUBIS_Nek5000
├── anubis_2017-01
│   ├── blame.json
│   ├── branch_status.json
│   ├── cloc.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-02
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-03
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
(...)
```

You can join the diffents files in the database with the CLI:

```bash
>anubis join-db ANUBIS_Nek5000
```

where `ANUBIS_Nek5000` is the name of the directory that contains your database.
After running the CLI, the directory should look like:

```bash
ANUBIS_Nek5000
├── anubis_2017-01
│   ├── blame.json
│   ├── branch_status.json
│   ├── cloc.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-02
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-03
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
(...)
├── joined_blame_monthly.json
├── joined_branch_status_monthly.json
├── joined_cloc_monthly.json
├── joined_commits_monthly.json
├── joined_lizard_monthly.json
```

Each `joined_*_monthly.json` contains the data for the whole period of time found in the directory.

For example, `joined_branch_status_monthly.json` looks like the following.

```json
[
    {
        "2017-02": [],
        "2017-05": [
            {
                "branch": "remotes/origin/openacc",
                "behind": 25,
                "ahead": 28,
                "nb_commits_ref_branch": 1494
            }
        ],
        "2018-10": [
            {
                "branch": "remotes/origin/openacc",
                "behind": 230,
                "ahead": 117,
                "nb_commits_ref_branch": 1699
            }
        ],
        "2017-04": [
            {
                "branch": "remotes/origin/openacc",
                "behind": 4,
                "ahead": 27,
                "nb_commits_ref_branch": 1473
            }
        ],
    }
]
```