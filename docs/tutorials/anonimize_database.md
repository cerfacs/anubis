
### Anonimize the database


You may want to anonimize your data to produce publicly available charts on your code.
`Anubis` allows you to replace commiters name by any other string.

To get the list of authors, use the CLI:

```bash
>anubis get-authors-list ANUBIS_Nek5000
```

It will create two files `list_authors_blame.json` and `list_authors_commits.json` that list every author that appears in `joined_blame_monthly.json` and `joined_commits_monthly.json`, respectively.

```bash
ANUBIS_Nek5000
├── anubis_2017-01
│   ├── blame.json
│   ├── branch_status.json
│   ├── cloc.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-02
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
├── anubis_2017-03
│   ├── cloc.json
│   ├── blame.json
│   ├── branch_status.json
│   ├── commits.json
│   └── lizard.csv
(...)
├── joined_blame_monthly.json
├── joined_branch_status_monthly.json
├── joined_cloc_monthly.json
├── joined_commits_monthly.json
├── joined_lizard_monthly.json
├── list_authors_blame.json
├── list_authors_commits.json
```

You can now edit the lists of authors to assign an identifier for each author key.
It is recommended to use trigram with first letter of the first name and first and last letter of the lastname.
For example `John Smith` becomes `JSH` in the `list_authors_blame.json`.

```json
{
    "John Smith": "JSH",
    "Prasad Nessa": "PNA",
    "Gunn Itsasne": "GIE",
    "Hana Scott": "HST",
    "Virginie Baudouin": "VBN",
    "Hugues Jessy": "HJY",
}
```

Once you have filled both files, you can replace authors by the corresponding joined file by using the following CLI:

```bash
>anubis use-trigram-for-blame ANUBIS_Nek5000 ANUBIS_Nek5000/list_authors_blame.json
>anubis use-trigram-for-commits ANUBIS_Nek5000 ANUBIS_Nek5000/list_authors_commits.json
```

`joined_blame_monthly_trigram.json` and `joined_commits_monthly_trigram.json` files are created.