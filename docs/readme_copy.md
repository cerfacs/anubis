
![Anubis](https://images.unsplash.com/photo-1595853899417-4cc421f2998e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2072&q=80)


Anubis is a codemetrics tool.



### Work in progress

Today, Anubis is essentially the script `timemachine.py`.
the time machine is a loop can spawn various versions  of a code from its gitbase, and perform analysis on it

### Installation

You need two external tools

- `git` obviously because we will mine information from the codebase
- `cloc` a Perl tool to count the line of codes. On my mac I use `brew install cloc`.  See [Source is on github](https://github.com/AlDanial/cloc).


### Usage

No CLI for now, comming soon.

I use `timemachine.py` with

```bash
python timemachine.py
```

where i control from the end of the script:
```python
main(
    "/Users/dauptain/GITLAB/avbp",
    rel_source_path="./SOURCES",
    branch="dev",
    year_start=2020,
    year_end=2022,
    out_dir="ANUBIS_AVBP2"
)
```

Have fun!!!

