### How to Anubis in easy steps

This small tutorial, that is still a work in progress will quickly guide you through the different steps, in order, to take to be able to complete a successfull anubis timemachine run as well as generating your first figures.


#### Step One: Copy of your repository

To avoid any unwanted suppression of file within your repository it is mandatory to do a git clone of the repository you want to analyze and avoid at all cost doing it in your work repository : 

```
git clone ..........
```

#### Step Two: Setting up the timemachine

In order to be able to complete a timemachine run you will have to fill the input file :

```
anubis anew

> Generating dummy inputfile ./anubis_time_machine.yml for anubis.
> File ./anubis_time_machine.yml created. Edit this file to set up your project...
```

You can now edit it and set your various parameters

```
# Anubis time machine input file

git_path : /home/code/repo
branch : master
rel_source_path : ./
year_start : 2020
year_end : 2020
out_dir : ANUBIS_OUT
lizard_switch : True
```

#### Step Three: Launching the timemachine

Up to this point if the input file is well completed everything shall will smoothly.

```
anubis timemachine -f ./anubis_time_machine.yml
```

***Note :** In this command line the -f is not mandatory if and only if the input file has this name and if it's the same folder as the one you're launching the timamachine command.* 

This is the kind of message that will be written in the terminal : 

```
Start anubis from command line on ./anubis_time_machine.yml

/Users/marzlin/bigbrother/ANUBIS_TEST2
Changing cwdir to /Users/marzlin/Gitlab/pyavbp/
2019 0
/Users/marzlin/bigbrother/ANUBIS_TEST2/anubis_2019-01
timewarping (1/24)...  date is now 2019-01-01T12:00:00
... find last revision
... checkout revision 234b2851995b991e09b82956a0004b4ff7ff7168
... gather commit stats
... get git blame info
... branch status
... git repository size
... running lizard

ETA : 0:01:25.380280 sec
2019 1
/Users/marzlin/bigbrother/ANUBIS_TEST2/anubis_2019-02
timewarping (2/24)...  date is now 2019-02-01T12:00:00
... find last revision
... checkout revision 2af1dfdd7a0620f75937414489be01929be22c2b
... gather commit stats
... get git blame info
... branch status
... git repository size
... running lizard

```

Further reding on how the databse is built can be found [here](../tutorials/generate_database.md).

#### Step Four: One database to rule them all

The next step is now to join the databse previously generated : 

```
anubis join-db ANUBIS_OUT_DIR/
```

This command line will join the various data contained in the various folders of your database in a .json file. Further reading on the database can be found [here](../tutorials/join_database).

#### Step Five: Generating authors list

The last step before generating the graphs is to generate the authors list database : 

```
anubis authors-list ANUBIS_OUT_DIR/
```

This will also generate a .json file containing the aliases for the various code authors, further reading can be found [here](../tutorial/anonimize_database.md)


#### Step Six: Generating the various figures

The last step if now to generate the various figures available through anubis cli. Please note that there is always an alternative to the .svg graphs by adding `--viz plotly` at the end of the command line.

```
anubis complexity ANUBIS_OUT_DIR/                         ## .svg 
anubis complexity ANUBIS_OUT_DIR/ --viz plotly            ## .html

anubis branches ANUBIS_OUT_DIR/                           ## .svg 
anubis branches ANUBIS_OUT_DIR/ --viz plotly              ## .html

anubis chronology ANUBIS_OUT_DIR/                         ## .svg (could take time)
anubis chronology ANUBIS_OUT_DIR/ --viz plotly            ## .html (could take time) 
```

TO BE CONTINUED ...