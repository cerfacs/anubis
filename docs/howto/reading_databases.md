
### Reading databases


The databases written by Anubis will be used in datavizualizations. 
Soon we will put here a nice example of vizualization.
For now here are some useful tricks 

##### Handling dates

The subfolders created by Anubis timemachine are something like `anubis_2019-11`.
Here is the way to load this date using the `datetime` package:


```python
import datetime
folder_date =  "anubis_2019-11"
def read_folderdate(fdate):
    str_date = folder_date.split("_")[1]
    date = datetime.datetime.strptime(str_date, "%Y-%m")
    return date
print(read_folderdate(folder_date))
```

Anubis also dig in the git repository, making a list of commits during one month.
For exemple I got `"Mon Jan 22 12:04:15 2018 +0100"`
Using `datetime` you can parse it this way:  

```python
import datetime
commit_date = "Mon Jan 22 12:04:15 2018 +0100"
def read_commitdate(cdate):
    date = datetime.datetime.strptime(cdate, "%c %z")
    return date
print(read_commitdate(commit_date))
```