""" File that gathers complexity plot only"""

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from math import log
from datetime import datetime

from tol_colors import tol_cset

from anubis.data_loader import load_jsons, load_tags
from anubis.timegraph import AnubisTimeGraph


# TODO : Move to a tool graph file
def normalize_score(
    in_: np.array, val_for_0: float, factor_for_10: int = 60
) -> np.array:
    """
    Normalize an array in_  into a score in range [0-10]

    Note:
        if in_ <= val_for_0  -> 0
        if in_ >= factor_for_10*val_for_0 -> 10

    Args:
        in_ (np.array): Array of score to normalize
        val_for_0 (float): 0 value threshold
        factor_for_10 (int, optionnal): Brutal, 10, Medium 40, Softie 100. Defaults to 60.
    """
    clip_ = np.clip(in_, val_for_0, None)
    alpha = 10.0 / log(factor_for_10)
    return alpha * np.log(clip_ / val_for_0)


def complexity_score(joined_complexity: dict) -> dict:
    """
    Return a dictionnary of various metrics for complexity

    Args:
        joined_complexity (dict): Complexity data acquired from the .json joined db.

    Returns:
        dict: Complexity metrics stored by dates
    """

    out_dict_ = {}
    for date, dict_ in joined_complexity.items():
        dict_np = {key: np.array(value) for key, value in dict_.items()}
        cc_ = normalize_score(dict_np["CCN"], 10.0)
        ci_ = normalize_score(dict_np["IDT"], 50.0)
        cs_ = normalize_score(dict_np["NLOC"], 50.0)
        cp_ = normalize_score(dict_np["param"], 5.0)
        score = (cc_ + ci_ + cs_ + cp_) / 4.0

        out_dict_[date] = {
            "score": score,
            "cyclomatic": cc_,
            "indentation": ci_,
            "params": cp_,
            "size": cs_,
        }

    return out_dict_


def complexity_exclude_patterns(
    joined_complexity: dict, exclude_patterns: list = None
) -> dict:
    """
    Filter the complexity join dictionnary from exclusion patterns

    Args:
        joined_complexity (dict): Complexity data acquired from the .json joined db.
        exclude_patterns (list], optional): List of patterns to exclude. Defaults to None.

    Returns:
        dict: Filtered complexity metrics stored by dates
    """

    f_complexity = dict()
    for date, dataset in joined_complexity.items():
        if len(dataset["file"]) == 0:
            continue  # skip if nothing to read

        inclusion_list = []
        for i, filename in enumerate(dataset["file"]):
            included = True
            for pattern in exclude_patterns:
                if pattern in filename:
                    included = False
            if included:
                inclusion_list.append(i)
        inclusion_mask = np.array(inclusion_list)
        try:
            f_dataset = {
                key: np.array(values)[inclusion_mask] for key, values in dataset.items()
            }
            f_complexity[date] = f_dataset
        except IndexError:
            # no match
            pass
    return f_complexity


def complexity_worst_performers(
    joined_complexity: dict, nfunc: int = 10, ctype: str = "score"
) -> dict:
    """
    Identify worst performers in complexity

    Args:
        joined_complexity (dict): Complexity data acquired from the .json joined db.
        nfunc (int, optional): Number of worst performers to keep. Defaults to 10.
        ctype (str, optional): Complexity metrics to target for ranking. Defaults to "score".

    Returns:
        dict: Top 10 dict stored by name
    """
    cplx_score = complexity_score(joined_complexity)

    dict_top10 = {}
    for date, dataset in joined_complexity.items():
        cc_com = cplx_score[date][ctype]
        cc_files = dataset["file"]
        cc_functions = dataset["function"]

        local_max = min(nfunc, cc_com.size - 1)
        mask_top10 = np.argpartition(cc_com, -local_max)[-local_max:]

        for index in mask_top10:
            name = f"{cc_files[index]}/\n{cc_functions[index]}"
            if name not in dict_top10:
                dict_top10[name] = {
                    "dates": [],
                    "score": [],
                }
            dict_top10[name]["dates"].append(date)
            dict_top10[name]["score"].append(cc_com[index])

    for name in dict_top10:
        dict_top10[name]["score"] = np.array(dict_top10[name]["score"])
    return dict_top10


def _create_complexity_plot_database(joined_complexity: dict) -> dict:
    """
    Function that handles the creation and organization of the database for the
    global complexity plot.
    Complexities are ponderated by NLOC

    Args:
        joined_complexity (dict): Complexity data acquired from the .json joined db

    Returns:
        (dict): dict with each complexity with their dates and values
    """

    # Gather only the last to date value of the complexity to arrange in croissant order
    complexity_dict = {
        "score": {"dates": [], "values": []},
        "indentation": {"dates": [], "values": []},
        "cyclomatic": {"dates": [], "values": []},
        "params": {"dates": [], "values": []},
        "size": {"dates": [], "values": []},
    }
    for date, complexities in complexity_score(joined_complexity).items():
        for complexity_name, complexity_val in complexities.items():
            nloc = joined_complexity[date]["NLOC"]
            if complexity_name in complexity_dict.keys():
                complexity_dict[complexity_name]["values"].append(
                    np.sum(complexity_val * nloc) / np.sum(nloc)
                )
                complexity_dict[complexity_name]["dates"].append(date)

    return complexity_dict


def create_worst_performers_database(dict_top10: dict, n_element: int) -> dict:
    """
    Function that handles the creation and organization of the database for the
    worts performers complexity plot. Aim to fully exploit seaborn lineplot.

    Args:
        dict_top10 (dict): Top 10 dict stored by name
        n_element (int): Number of worst performers to keep.

    Returns:
        (dict): Flat dict ordered by key with the lowest values to the highest
    """

    # Ordering the previous dict
    contestants = {}
    for file in dict_top10.keys():
        contestants[file] = dict_top10[file]["score"][-1]
    contestants = dict(sorted(contestants.items(), key=lambda item: item[1]))

    # iteration in reverse, so If we end up with too many performers,
    # the olders are skipped
    dict_worst_performers = {}
    for idx, name in enumerate(reversed(list(contestants.keys()))):
        if idx > n_element - 1:
            break
        dict_worst_performers[name] = {"dates": [], "score": []}
        dict_worst_performers[name]["dates"].extend(dict_top10[name]["dates"])
        dict_worst_performers[name]["score"].extend(dict_top10[name]["score"])

    return dict_worst_performers


def plot_complexity(
    folder: str = "./",
    ctype: str = "score",
    date_start: str = None,
    date_end: str = None,
    exclude_patterns: list = None,
) -> plt.Axes:
    """
    The plots of complexity along time

    Args:
        folder (str, optional): Path to the source folder. Defaults to "./".
        ctype (str, optional): Complexity metrics to target for ranking. Defaults to "score".
        date_start (str, optional): Date start included, using strptime format %Y-%m-%d. Defaults to None.
        date_end (str, optional): Date end included, using strptime format %Y-%m-%d. Defaults to None.
        exclude_patterns (list, optional): List of patterns to exclude. Defaults to None.

    Returns:
        plt.Axes: Anubis Timegraph Axes object
    """

    joined_complexity = complexity_exclude_patterns(
        load_jsons(folder, "complexity.json", date_start=date_start, date_end=date_end),
        exclude_patterns=exclude_patterns,
    )
    dates = list(joined_complexity.keys())
    complexity_dict = _create_complexity_plot_database(joined_complexity)
    sorted_complexity_dict = {
        k: v
        for k, v in sorted(
            complexity_dict.items(),
            key=lambda item: item[1]["values"][-1],
            reverse=True,
        )
    }

    abt = AnubisTimeGraph(title="Global Complexity")
    abt.ax.set_ylabel(
        f"{ctype.capitalize()} - 0 (good) to 10 (bad)", fontsize=20, labelpad=10
    )
    cset = tol_cset("muted")
    abt.create_lineplot(sorted_complexity_dict, "dates", "values", cset)

    abt.ax.set_ylim(0, 10)

    # Ensure that the EOL are well located
    x_max = datetime(1800, 1, 1)
    for main_key in sorted_complexity_dict.keys():
        x_max = max(x_max, sorted_complexity_dict[main_key]["dates"][-1])

    abt.eol_values(x_max)
    abt.ax.legend()
    sns.move_legend(
        abt.ax,
        "upper center",
        bbox_to_anchor=(0.5, 1.05),
        ncol=5,
        title=None,
        frameon=False,
        fontsize=17,
    )
    abt.add_tags(load_tags(folder))
    sns.despine()

    return abt.ax


def plot_worst_performers(
    folder: str = "./",
    ctype: str = "score",
    date_start: str = None,
    date_end: str = None,
    nfunc: int = 10,
    exclude_patterns: list = None,
) -> plt.Axes:
    """
    Plot the worst performers in the code

    Args:
        folder (str, optional): Path to the source folder. Defaults to "./".
        ctype (str, optional): Complexity metrics to target for ranking. Defaults to "score".
        date_start (str, optional): Date start included, using strptime format %Y-%m-%d. Defaults to None.
        date_end (str, optional): Date end included, using strptime format %Y-%m-%d. Defaults to None.
        nfunc (int, optional): Number of worst performers to keep. Defaults to 10.
        exclude_patterns (list, optional): List of patterns to exclude. Defaults to None.

    Returns:
        plt.Axes: Anubis Timegraph Axes object
    """
    joined_complexity = complexity_exclude_patterns(
        load_jsons(folder, "complexity.json", date_start=date_start, date_end=date_end),
        exclude_patterns=exclude_patterns,
    )

    dict_top10 = complexity_worst_performers(
        joined_complexity,
        nfunc=nfunc,
    )

    dict_worst_performers = create_worst_performers_database(dict_top10, nfunc)
    cset = tol_cset("muted")

    abt = AnubisTimeGraph(title="Worst performers")
    abt.ax.set_ylabel(
        f"{ctype.capitalize()} - 0 (good) to 10 (bad)", fontsize=20, labelpad=10
    )
    abt.create_lineplot(dict_worst_performers, "dates", "score", cset)

    abt.ax.set_ylim(0, 10)

    # Ensure that the EOL are well located
    x_max = datetime(1800, 1, 1)
    for main_key in dict_worst_performers.keys():
        x_max = max(x_max, dict_worst_performers[main_key]["dates"][-1])
    abt.eol_values(x_max)

    abt.add_tags(load_tags(folder))
    abt.ax.legend()
    sns.move_legend(
        abt.ax,
        "lower center",
        bbox_to_anchor=(0.5, -0.3),
        ncol=3,
        title=None,
        frameon=False,
        fontsize=12,
    )
    sns.despine()

    return abt.ax
