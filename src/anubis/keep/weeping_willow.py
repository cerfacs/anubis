from datetime import datetime, timedelta

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tol_colors as tc
from matplotlib.patches import Rectangle
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def plot_branch_history(
    data_path: str,
    master_name: str,
    code_name: str,
    filters: dict = None,
    colormap: str = "rainbow_PuBr",
    save_path: str = "",
):
    """Plot branch history from anubis data

    save the figure

    Args:
        data_path (str): path of joined_branch_status_monthly.json
        master_name (str): master branch name
        code_name (str): code name
        filters (dict): filters for plotting branches. Accepted keys:
            - name_contains (list of str): remove branches that contains the string
        colormap (str): Paul Tol compliant colormap
        save_path (str): path to save the figure branches_history_{code_name}.png
    """
    # 0. Init filters
    if filters is None:
        filters = {}

    # 1. Load data
    branch_path = f"{data_path}/joined_branch_status_monthly.json"
    try:
        raw_data = pd.read_json(branch_path)
    except ValueError as e:
        print(
            f"ERROR: joined_branch_status_monthly.json not found in {data_path} in function plot_branch_history"
        )
        raise (e)

    # 2. Parse data to get branches names and master branch commits
    # tmp branch behind data
    master_commits_tmp = []
    # branch names
    branches_names = []
    for idx, date in enumerate(raw_data):
        if date < datetime.now() and len(raw_data[list(raw_data)[idx]][0]) > 0:
            dict_list = raw_data[list(raw_data)[idx]][0]
            # use only first branch by date to get numbers of commits on master
            master_commits_tmp.append([np.datetime64(date), dict_list[0]["nb_commits_ref_branch"]])
            for branch in dict_list:
                if branch["branch"] not in branches_names:
                    branches_names.append(branch["branch"])

    tmp_array = np.array(master_commits_tmp)
    # sort by date
    master_commits = tmp_array[tmp_array[:, 0].argsort()]

    # print(branches_names)

    # 3. Parse data to get branches behind and ahead
    # time/branch/index correspondance for array
    dict_index_time = {}
    for i, time in enumerate(master_commits[:, 0]):
        dict_index_time[time] = i
    dict_index_branch = {}
    for i, time in enumerate(branches_names):
        dict_index_branch[time] = i

    # allocate arrays
    N_time = np.size(master_commits, 0)
    N_branches = len(branches_names)
    branch_behind = np.zeros((N_branches, N_time))
    branch_ahead = np.zeros((N_branches, N_time))
    # init nan
    branch_behind[:, :] = np.nan
    branch_ahead[:, :] = np.nan

    # parse data
    for idx, date in enumerate(raw_data):
        # get data for past date only
        if date < datetime.now():
            dict_list = raw_data[list(raw_data)[idx]][0]
            for branch_info in dict_list:
                tmp_branch_name = branch_info["branch"]
                tmp_branch_time = np.datetime64(date)
                # get behind and ahead data
                behind = branch_info["nb_commits_ref_branch"] - branch_info["behind"]
                ahead = branch_info["ahead"]
                # store at the right place in the array
                branch_behind[dict_index_branch[tmp_branch_name], dict_index_time[tmp_branch_time]] = behind
                branch_ahead[dict_index_branch[tmp_branch_name], dict_index_time[tmp_branch_time]] = ahead

    # 4. Apply filters
    nb_ignored_branches = 0
    nb_filtered_branches = 0

    # list of selected branches index
    selected_branches = []

    # get name_contains filter out list
    if "name_contains" in filters.keys():
        filter_name_contains = filters["name_contains"]
    else:
        filter_name_contains = []

    # get name_contains filter list
    if "expiration_date" in filters.keys():
        expiration_date = filters["expiration_date"]
    else:
        expiration_date = None

    # get name_contains filter in list
    if "name_with" in filters.keys():
        filter_name_with = filters["name_with"]
    else:
        filter_name_with = []

    # apply filters
    for b in range(N_branches):
        need_check = True

        # master branch filter
        if branches_names[b].replace("remotes/origin/", "") in [master_name]:
            break

        # name contains
        for name in filter_name_contains:
            if name in branches_names[b]:
                nb_filtered_branches += 1
                need_check = False
                break

        # expiration date
        if expiration_date is not None:
            # not moving branch for over than expiration_date are considered stale
            if (
                np.nanstd(branch_behind[b, -expiration_date:]) == 0
                and np.nanstd(branch_ahead[b, -expiration_date:]) == 0
            ):
                nb_ignored_branches += 1
                need_check = False

        # name contains
        for name in filter_name_with:
            if name not in branches_names[b]:
                nb_filtered_branches += 1
                need_check = False
                break

        # stale branches
        if need_check:
            # branch too far behind master
            if np.max(branch_behind[b, :] < master_commits[0, 1]):
                nb_ignored_branches += 1
            else:
                selected_branches.append(b)

    # 5. Sort selected branches to use proper colors
    colors = tc.tol_cmap(colormap)(np.linspace(0, 1, len(selected_branches)))

    tmp_array = []
    for k, b in enumerate(selected_branches):
        tmp_array.append([k, b, branch_behind[b, -1]])

    branch_name_pos = np.array(tmp_array)
    branch_name_pos = branch_name_pos[branch_name_pos[:, 2].argsort()]
    # position of branch names in y direction
    branch_legend_pos_y = np.linspace(
        np.nanmin(master_commits[:, 1]), np.nanmax(master_commits[:, 1]), len(selected_branches)
    )

    # 6. Get various info
    # dev rate for master branch
    mean_rate_master = np.mean(np.diff(master_commits[:, 1]))

    # 7. Plot
    ## 7.1 Main figure info

    fig, ax = plt.subplots(1, 1, figsize=(12, 6), sharex=True)
    ax.set_xlabel("time")
    ax.set_ylabel("# commits")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.tick_params(axis="y", direction="in")
    ax.tick_params(axis="x", direction="in")

    ## 7.2 Global info
    ax.text(
        0.5,
        0.03,
        f"+ {nb_ignored_branches} stale branches & {nb_filtered_branches} filtered branches",
        horizontalalignment="center",
        verticalalignment="center",
        transform=ax.transAxes,
        fontsize=8,
    )

    ax.text(
        0.5,
        0.99,
        f"rate of dev on master: {mean_rate_master:.0f} commits/month",
        horizontalalignment="center",
        verticalalignment="center",
        transform=ax.transAxes,
        fontsize=9,
    )

    ax.set_title(f"master branch: {master_name}")

    ## 7.3 Master plot
    ax.plot(mdates.date2num(master_commits[:, 0]), master_commits[:, 1], "k", lw=3)
    ax.text(
        master_commits[int(N_time / 2), 0].astype(datetime),
        1.01 * master_commits[int(N_time / 2), 1],
        f"master",
        horizontalalignment="right",
        verticalalignment="bottom",
        c="k",
        fontsize=10,
        weight="bold",
    )

    ## 7.4 branch lines and ahead rectangles
    # base width for plot
    days = 15
    # set rectangle width
    rec_width = timedelta(days=days)
    rec_width_half = timedelta(days=int(days / 2))
    # set space for branch legend
    text_width = timedelta(days=int(5 * days))
    textdate = master_commits[-1, 0].astype(datetime) + text_width
    arrowdate = master_commits[-1, 0].astype(datetime) + rec_width

    # plot
    for a, branchinfo in enumerate(branch_name_pos):
        # index for color
        k = int(branchinfo[0])
        # index of branch in global array
        b = int(branchinfo[1])
        # branch name
        branch_name_b = branches_names[b].replace("remotes/origin/", "")

        # plot behind line
        ax.plot(master_commits[:, 0], branch_behind[b, :], lw=1, c=colors[a], alpha=0.3)

        # ahead rectangle
        for t in range(N_time):
            if branch_ahead[b, t] != 0:
                # if ahead is not null
                left_date = master_commits[t, 0].astype(datetime) - rec_width_half
                height = branch_ahead[b, t]
                start = branch_behind[b, t] - height
                rect = Rectangle((left_date, start), rec_width, height, color=colors[a], alpha=0.3)
                ax.add_patch(rect)

        # legend of branches
        ax.annotate(
            text=f"{branch_name_b}",
            xycoords="data",
            textcoords="data",
            xy=(arrowdate, branchinfo[2]),
            xytext=(textdate, branch_legend_pos_y[a]),
            arrowprops=dict(
                arrowstyle="-",
                color=colors[a],
                relpos=(0.0, 0.5),
            ),
            color=colors[a],
            ha="left",
            va="center_baseline",
            fontsize=8,
        )

    # 7.5 legend
    # fixed size sub axe
    axins = inset_axes(ax, width=2, height=2, loc=2)
    # create master branch
    master_tmp = np.array([1, 8, 29, 43, 50, 57, 68, 75, 90, 93, 97])
    # create behind branch data
    branch_behind_tmp = np.array([np.nan, np.nan, 29, 29, 50, 50, 50, 50, 65, 65, 65])
    # plot lines
    axins.plot(
        np.linspace(0, 1, 11),
        master_tmp,
        "k",
        lw=3,
        zorder=0,
    )
    axins.plot(
        np.linspace(0, 1, 11),
        branch_behind_tmp,
        "b",
        lw=1,
        alpha=0.3,
        zorder=0,
    )
    # plot ahead rectangles
    # R1
    rect = Rectangle((0.27, 26), 0.06, 3, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)
    # R2
    rect = Rectangle((0.37, 47), 0.06, 3, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)
    # R3
    rect = Rectangle((0.47, 44), 0.06, 6, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)
    # R4
    rect = Rectangle((0.57, 41), 0.06, 9, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)
    # R5
    rect = Rectangle((0.67, 35), 0.06, 15, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)
    # R6
    rect = Rectangle((0.97, 62), 0.06, 3, color="b", alpha=0.3, zorder=1)
    axins.add_patch(rect)

    # legends
    t = axins.text(
        0.58,
        59,
        f"behind",
        horizontalalignment="right",
        verticalalignment="center",
        c="gray",
        backgroundcolor="white",
        fontsize=8,
    )
    t.set_bbox(dict(facecolor="white", alpha=0.9, edgecolor="white", linewidth=0))

    axins.annotate(
        text="",
        xy=(0.76, 35),
        xytext=(0.76, 50),
        arrowprops=dict(arrowstyle="<->", shrinkA=0, shrinkB=0, color="gray"),
    )
    axins.annotate(
        text="",
        xy=(0.6, 50),
        xytext=(0.6, 68),
        arrowprops=dict(arrowstyle="<->", shrinkA=0, shrinkB=0, color="gray"),
    )

    axins.annotate(
        text="merge\nmaster --> branch",
        xy=(0.35, 39.5),
        xytext=(0.5, 5),
        arrowprops=dict(arrowstyle="-|>", color="gray"),
        fontsize=8,
        color="gray",
        ha="center",
        va="bottom",
    )

    axins.annotate(
        text="merge\nbranch --> master",
        xy=(0.75, 57.5),
        xytext=(0.70, 85),
        arrowprops=dict(arrowstyle="-|>", color="gray", relpos=(0.9, 0)),
        fontsize=8,
        color="gray",
        ha="right",
        va="bottom",
    )

    axins.text(
        0.77,
        42.5,
        f"ahead",
        horizontalalignment="left",
        verticalalignment="center",
        c="gray",
        fontsize=8,
    )

    x1, x2, y1, y2 = 0, 1, 0, 100
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    axins.set_xticklabels([])
    axins.set_yticklabels([])
    axins.tick_params(axis="y", left=False)
    axins.tick_params(axis="x", bottom=False)
    axins.set_title("Example", fontsize=8)

    ## 7.6 Save
    plt.tight_layout()
    if save_path == "":
        plt.savefig(f"branches_history_{code_name}.png", dpi=200)
    else:
        plt.savefig(f"{save_path}/branches_history_{code_name}.png", dpi=200)