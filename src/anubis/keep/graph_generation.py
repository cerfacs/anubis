import matplotlib.pyplot as plt


from codemetrics_tools import (
    dataframe_joined_commits,
    dataframe_mean_commits,
    commits_background,
    commits_nbr_pie_chart,
    churn_pie_chart,
    get_author_names,
    dataframe_churn,
    hist_churn,
    dataframe_joined_blame,
    dataframe_code_age,
    get_dict_code_age,
    historic_stackplot,
    commits_zoology,
    dataframe_churn_cloud,
    churn_cloud_background,
    churn_cloud_scatter,
    blame_age_plot,
)


## COMMITS

df_commits = dataframe_joined_commits(
    "/Users/marzlin/Codemetrics/CEFORA/ANUBIS_MNH/joined_commits_monthly_trigram.json"
)

# PIE CHARTS
ax = commits_nbr_pie_chart(df_commits)
ax.figure.savefig("piechart_commits_nbr_mnh.png")
plt.show()

ax = churn_pie_chart(df_commits)
ax.figure.savefig("piechart_churn_mnh.png")
plt.show()

# CHURNS
df_churn = dataframe_churn(df_commits)
ax = hist_churn(df_churn)
ax.figure.savefig("hist_churn_mnh.png")
plt.show()

df_mean_cloud = dataframe_mean_commits(df_commits)
ax_background = commits_background(df_commits)
ax = commits_zoology(df_commits, df_mean_cloud, ax_background)
ax.figure.savefig("commits_zoology_mnh.png")
plt.show()

names = get_author_names(
    "/Users/marzlin/Codemetrics/CEFORA/ANUBIS_MNH/list_authors_commits.json"
)
df_churn_cloud = dataframe_churn_cloud(df_commits, names)
ax_background = churn_cloud_background(df_churn_cloud)
ax = churn_cloud_scatter(df_churn_cloud, ax_background)
ax.figure.savefig("churn_cloud_mnh.png")
plt.show()

## BLAME

df_blame = dataframe_joined_blame(
    "/Users/marzlin/Codemetrics/CEFORA/ANUBIS_MNH/joined_blame_monthly_trigram.json"
)

# AGE EVOLUTION

ax = blame_age_plot(df_blame)
ax.figure.savefig("blame_age_mnh.png")
plt.show()

# CODE AGE
df_code_age = dataframe_code_age(df_blame)
dict_code_age = get_dict_code_age(df_code_age)
ax = historic_stackplot(df_code_age, dict_code_age, 0.1)
ax.figure.savefig("sedimentation_mnh.png")
plt.show()
