import json
from datetime import date, datetime
from functools import reduce


import matplotlib.pyplot as plt
from matplotlib.axes._axes import Axes as MplAxes
from matplotlib.ticker import FixedLocator, FixedFormatter
import matplotlib.dates as mdates

import seaborn as sns
import pandas as pd
import numpy as np

from anubis.tol_colors import tol_cmap



parameters = {
    "colors": ["#77AADD", "#EE8866", "#44BB99", "#EE8866", "#FFAABB"],
    "labels": [
        "Mostly Insertions",
        "Refactoring",
        "Linting",
        "Refactoring",
        "Mostly Deletions",
    ],
    "under_thresholds": [np.inf, 2, 1.2, 1, 0.5],
    "Mostly Insertions": "#77AADD",
    "Refactoring": "#EE8866",
    "Linting": "#44BB99",
    "Mostly Deletions": "#FFAABB",
}


def dataframe_joined_commits(path: str) -> pd.DataFrame:
    """
    Function that reads the joined_commits.json and returns a dataframe.

    Args:
        path (str): Path with filename

    Returns:
        df_commits (obj): Pandas dataframe object
    """
    commits_data = pd.read_json(path)

    list_data = []
    for idx, _ in enumerate(commits_data):
        dict_list = commits_data[list(commits_data)[idx]][0]

        if type(dict_list) is list:

            for dicts in dict_list:
                flatten_dict = {}
                dicts["date"] = list(commits_data)[idx]
                flatten_dict.update(dicts)
                list_data.append(flatten_dict)

    df_commits = pd.DataFrame(list_data)
    df_commits = df_commits.dropna()

    df_commits["churn"] = df_commits[["insertions", "deletions"]].sum(axis=1)
    df_commits["ratio"] = df_commits["insertions"] / df_commits["deletions"]

    df_commits["type"] = parameters["labels"][0]
    for index, names in enumerate(parameters["labels"]):
        df_commits.loc[
            df_commits.ratio <= parameters["under_thresholds"][index], "type"
        ] = names

    return df_commits


def dataframe_mean_commits(df_commits: pd.DataFrame) -> pd.DataFrame:
    """
    Function that returns a pandas dataframe from the commits dataframe.
    The mean value of insertions and deletions is computed for each author as well
    as the number of commits.

    Args:
        dataframe (obj): Pandas Dataframe from the commits

    Returns:
        df_mean (obj): Pandas Dataframe with the mean values
    """

    df_mean = (
        df_commits.groupby("author")
        .agg(
            commits_nbr=("files", "count"),
            mean_insertions=("insertions", "mean"),
            mean_deletions=("deletions", "mean"),
        )
        .reset_index()
    )
    return df_mean


def dataframe_churn_cloud(df_commits: pd.DataFrame, names: set) -> pd.DataFrame:
    """
    Function that returns a pandas dataframe with the number of lines updated
    by an author, the number of commits and the age of the author since his/her first
    commit.

    Args:
        df_commits (obj): Pandas Dataframe from the commits
        names (set); Set of the author names

    Returns:
        df_churn_cloud (obj): Pandas Dataframe with age and churn data
    """

    df_churn_cloud = (
        df_commits.groupby("author")
        .agg(commits_nbr=("churn", "count"), churn_sum=("churn", "sum"))
        .reset_index()
    )

    for author_name in names:
        df_commits_author = df_commits[df_commits.author == author_name]
        age = (max(df_commits.date) - min(df_commits_author.date)) / np.timedelta64(
            1, "M"
        )
        df_churn_cloud.loc[df_churn_cloud.author == author_name, "age"] = age

    return df_churn_cloud


def dataframe_churn(df_commits: pd.DataFrame) -> pd.DataFrame:
    """
    Function that take your commits dataframe and add the type of commits to it
    if it's a refactoring, linting etc.

    Args:
        df_commits (obj): Pandas Dataframe of joined commits

    Returns:
        df_churn (obj): Pandas Dataframe of joined commits with churn values added for the commit type
    """

    df_commits = df_commits.drop(
        ["author", "files", "insertions", "deletions", "revision", "ratio"], axis=1
    )
    df_churn = df_commits.groupby(["date", "type"])["churn"].sum().reset_index()

    df_list = []
    for commit_type in set(parameters["labels"]):
        df_type = df_churn[df_churn.type == commit_type]
        df_type = df_type.rename(columns={"churn": commit_type})
        df_type = df_type.drop(columns="type")
        df_list.append(df_type)

    df_churn = reduce(
        lambda x, y: pd.merge(x, y, on="date", how="outer"), df_list
    ).fillna(0)
    df_churn = df_churn.sort_values("date")
    df_churn = df_churn.set_index("date")

    return df_churn


def dataframe_joined_blame(path: str) -> pd.DataFrame:
    """
    Function that reads the joined_blame.json and returns a dataframe.

    Args:
        path (str): Path with filename

    Returns:
        df_blame (obj): Pandas dataframe object
    """
    blame_PYAVBP = pd.read_json(path)

    data_blame = []
    for idx, folder_date in enumerate(blame_PYAVBP):
        dict_list = blame_PYAVBP[list(blame_PYAVBP)[idx]][0]
        for values in dict_list:
            if len(values) > 0:
                for num, names in enumerate(values["author"]):
                    flatten_dict = {}
                    flatten_dict["date"] = folder_date
                    flatten_dict["file"] = values["file"]
                    flatten_dict["author"] = names
                    flatten_dict["line_last_update"] = values["date"][num]
                    flatten_dict["indentation"] = values["indentation"][num]
                    flatten_dict["line_number"] = values["line_number"][num]
                    data_blame.append(flatten_dict)
            else:
                pass

    df_blame = pd.DataFrame(data_blame)
    df_blame = df_blame.dropna()

    df_blame = df_blame.sort_values(["date", "line_last_update"])
    df_blame["date"] = pd.to_datetime(df_blame["date"])
    df_blame["line_last_update"] = pd.to_datetime(df_blame["line_last_update"])
    df_blame["age"] = (df_blame.date - df_blame.line_last_update) / np.timedelta64(
        1, "M"
    )
    df_blame["line_last_update"] = df_blame.line_last_update.dt.to_period("M")

    current_date = date.today().strftime("%Y-%m-%d")
    df_blame = df_blame[~(df_blame["date"] > current_date)]

    return df_blame


def dataframe_code_age(df_blame: pd.DataFrame) -> pd.DataFrame:
    """
    Function that creates a dataframe for gathering the dataframe informations that
    will be used to create a stackplot.

    Args:
        df_blame (obj): Pandas dataframe from blame

    Returns:
        df_code_age (obj): Pandas dataframe of the code age
    """
    df_paleo = (
        df_blame.groupby(["date", "line_last_update"])
        .agg(
            line_number=("line_last_update", "count"),
        )
        .reset_index()
    )
    df_linesnumber = (
        df_blame.groupby("date")
        .agg(total_of_lines=("line_number", "count"))
        .reset_index()
    )

    df_code_age = pd.merge(df_paleo, df_linesnumber, on="date", how="outer", copy=True)
    df_code_age["ratio"] = df_code_age["line_number"] / max(
        df_code_age["total_of_lines"]
    )

    return df_code_age


def get_dict_code_age(df_code_age: pd.DataFrame) -> dict:
    """
    Function that creates the dict used to create the stackplot.

    Args:
        df_code_age (obj): Pandas dataframe generated by the dataframe_code_age

    Returns:
        dict_code_age (dict): dict from dataframe of the code age


    """
    y_linedate = np.array(sorted(set(df_code_age.line_last_update)))

    dict_code_age = {}
    for linedate in y_linedate:
        dict_code_age[linedate] = []

    x_date = np.array(sorted(set(df_code_age["date"].to_numpy())))
    for date in x_date:
        df_sample = df_code_age.loc[lambda df: df["date"] == date]
        for linedate in y_linedate:
            if linedate in df_sample["line_last_update"].values:
                dict_code_age[linedate].append(
                    df_sample.loc[lambda df: df["line_last_update"] == linedate][
                        "line_number"
                    ].values[0]
                )
            else:
                dict_code_age[linedate].append(0)

    for key in dict_code_age.keys():
        dict_code_age[key] = np.array(dict_code_age[key])

    return dict_code_age


def get_lines_date(df_code_age: pd.DataFrame, threshold: float) -> dict:
    """
    Function that retruns a dict with the date as key and a tuple of coordinates
    to be written on the stackplot if number of lines at this date is beyond the threshold.

    Args:
        df_code_age (obj): Pandas dataframe from code_age_dataframe
        threshold (float): Value between 0 and 1, fraction of the max_lines above which
                        the date will be written on the graph.
    Returns:
        date_to_write (dict): dict of dates with coordinates
    """

    date_to_write = {}
    for idx, date in enumerate(df_code_age.date):
        df_sample = df_code_age[df_code_age["date"] == date]
        df_sample.insert(5, "cumulative", df_sample.line_number.cumsum(), True)
        if df_sample["ratio"].iloc[-1] > threshold:
            if len(df_sample) > 1:
                y_pos = (
                    df_sample["cumulative"].iloc[-1] + df_sample["cumulative"].iloc[-2]
                ) / 2
                date_to_write[df_sample["line_last_update"].iloc[-1]] = (
                    date.to_numpy(),
                    y_pos,
                )

    return date_to_write


def commits_nbr_pie_chart(df_commits: pd.DataFrame) -> MplAxes:
    """
    Function that returns the pie chart matplotlib axe of the commits number repartition

    Args:
        df_commits (obj): Pandas dataframe with the commits data.

    Returns:
        ax (obj): matplotlib.axes._axes object
    """
    df_pie = (
        df_commits.groupby("type").agg(commits_nbr=("revision", "count")).reset_index()
    )

    labels = sorted(list(df_pie.type))
    colors = []
    for label in labels:
        colors.append(parameters[label])

    fig, ax = plt.subplots(figsize=(15, 10))
    fig.set_facecolor("#FFFFFF")
    ax.pie(df_pie.commits_nbr, labels=labels, colors=colors, textprops={"fontsize": 20})
    return ax


def churn_pie_chart(df_commits: pd.DataFrame) -> MplAxes:
    """
    Function that returns the pie chart matplotlib axe of the commits churn repartition

    Args:
        df_commits (obj): Pandas dataframe with the commits data.

    Returns:
        ax (obj): matplotlib.axes._axes object
    """
    df_pie = df_commits.groupby("type").agg(churn_sum=("churn", "sum")).reset_index()
    labels = sorted(list(df_pie.type))
    colors = []
    for label in labels:
        colors.append(parameters[label])

    fig, ax = plt.subplots(figsize=(15, 10))
    fig.set_facecolor("#FFFFFF")
    ax.pie(df_pie.churn_sum, labels=labels, colors=colors, textprops={"fontsize": 20})

    return ax


def commits_background(df_commits: pd.DataFrame) -> MplAxes:
    """
    Function that returns the matplotlib axe for the backgroung of
    the commits zoology graph.

    Args:
        df_commits (obj): Pandas Dataframe from the commits

    Returns:
        ax (obj): Matplotlib axe of the background
    """
    f, ax = plt.subplots(figsize=(20, 15))

    base = [1, max(df_commits.deletions)]
    pure_deletions = [
        parameters["under_thresholds"][-1],
        max(df_commits.insertions) * parameters["under_thresholds"][-1],
    ]
    refactor1 = [
        parameters["under_thresholds"][-2],
        max(df_commits.insertions) * parameters["under_thresholds"][-2],
    ]
    linting = [
        parameters["under_thresholds"][-3],
        max(df_commits.insertions) * parameters["under_thresholds"][-3],
    ]
    refactor2 = [
        parameters["under_thresholds"][-4],
        max(df_commits.insertions) * parameters["under_thresholds"][-4],
    ]
    pure_insertions = [1e8, 1e8]

    ax = plt.fill_between(
        base, pure_deletions, alpha=1, zorder=1, color=parameters["colors"][4]
    )
    ax = plt.fill_between(
        base,
        pure_deletions,
        refactor1,
        alpha=1,
        zorder=1,
        color=parameters["colors"][3],
    )
    ax = plt.fill_between(
        base, refactor1, linting, alpha=1, zorder=1, color=parameters["colors"][2]
    )
    ax = plt.fill_between(
        base, linting, refactor2, alpha=1, zorder=1, color=parameters["colors"][1]
    )
    ax = plt.fill_between(
        base,
        refactor2,
        pure_insertions,
        alpha=1,
        zorder=1,
        color=parameters["colors"][0],
    )

    return ax


def churn_cloud_background(df_churn_cloud: pd.DataFrame) -> MplAxes:
    """
    Function that returns the matplotlib axe for the backgroung of
    the churn_cloud graph.

    Args:
        df_churn_cloud (obj): Pandas Dataframe from the churn_cloud

    Returns:
        ax (obj): Matplotlib axe of the background
    """
    f, ax = plt.subplots(figsize=(15, 10))

    base = [1e0, max(df_churn_cloud.commits_nbr) * 1.5]
    lims1 = [1, max(df_churn_cloud.commits_nbr)]
    lims10 = [10, max(df_churn_cloud.commits_nbr) * 10]
    lims100 = [100, max(df_churn_cloud.commits_nbr) * 100]
    lims1000 = [1000, max(df_churn_cloud.commits_nbr) * 1000]

    ax = plt.fill_between(base, lims10, alpha=1, zorder=1, color="#BBCCEE")
    ax = plt.fill_between(base, lims10, lims100, alpha=1, zorder=1, color="#CCEEFF")
    ax = plt.fill_between(base, lims100, lims1000, alpha=1, zorder=1, color="#CCDDAA")
    ax = plt.fill_between(
        base, lims1000, [1e8, 1e8], alpha=1, zorder=1, color="#EEEEBB"
    )

    return ax


def churn_cloud_scatter(df_churn_cloud: pd.DataFrame, ax: MplAxes) -> MplAxes:
    """
    Function that returns the commits zoology matplotlib axe with the scatter plots.

    Args:
        df_churn_cloud (obj): Pandas dataframe of the churn cloud
        ax (obj): Matplotlib axe of the churn_cloud background

    Retuns:
        ax (obj): Matplotlib axe of the commits zoology
    """
    ax = sns.scatterplot(
        x="commits_nbr",
        y="churn_sum",
        data=df_churn_cloud,
        color="None",
        edgecolor="black",
        s=100 * np.sqrt(df_churn_cloud["age"]),
        alpha=1,
        legend=False,
        markers="o",
    )

    # Adding labels, changing scales and limits
    ax.set_xlabel(xlabel="Sum of commits per author", fontsize=25)
    ax.set_ylabel(ylabel="Sum of lines added and deleted per author", fontsize=25)
    ax.tick_params(axis="y", labelsize=25)
    ax.tick_params(axis="x", labelsize=25)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim([1e0, max(df_churn_cloud.commits_nbr) * 1.5])
    ax.set_ylim([1e0, max(df_churn_cloud.churn_sum) * 1.5])

    # Adding trigram close to its point
    for i in range(df_churn_cloud.shape[0]):
        plt.text(
            x=df_churn_cloud.commits_nbr[i] * 1.1,
            y=df_churn_cloud.churn_sum[i] * 0.9,
            s=df_churn_cloud.author[i],
            fontdict=dict(color="black", size=20),
        )

    # Adding two texts for location
    plt.text(
        x=4e0,
        y=max(df_churn_cloud.churn_sum) * 0.6,
        s="Big commits",
        fontdict=dict(color="black", size=25),
    )
    plt.text(x=20, y=10, s="Small commits", fontdict=dict(color="black", size=25))

    # Adding manually legend title
    plt.text(
        x=3,
        y=1.6,
        s="Author activity (Months): ",
        fontdict=dict(color="black", size=25),
    )

    # Adding circle surfaces scale onto legend
    for age in [2, 4, 8, 16, 32, 64]:
        plt.scatter(
            [],
            [],
            color="None",
            edgecolors="black",
            alpha=1,
            s=100 * np.sqrt(age),
            label=f"{age} ",
        )
    # plt.legend(scatterpoints=1, frameon=False, labelspacing=2)
    ax.legend(
        loc="center right",
        bbox_to_anchor=(1, 0.05),
        ncol=6,
        labelspacing=0.5,
        columnspacing=1,
        frameon=False,
        scatteryoffsets=[1.0, 0.0, 0.0],
    )
    plt.setp(ax.get_legend().get_texts(), fontsize="25")

    # Changing y-axis and x-axis ticks

    y_formatter = FixedFormatter(["1", "10", "100", "1000", "10000", "100000"])
    y_locator = FixedLocator([1, 10, 100, 1000, 10000, 100000])
    ax.yaxis.set_major_formatter(y_formatter)
    ax.yaxis.set_major_locator(y_locator)

    x_formatter = FixedFormatter(["1", "10", "100", "1000"])
    x_locator = FixedLocator([1, 10, 100, 1000])
    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)

    return ax


def commits_zoology(
    df_commits: pd.DataFrame, df_mean: pd.DataFrame, ax: MplAxes
) -> MplAxes:
    """
    Function that returns the commits zoology matplotlib axe with the scatter plots.

    Args:
        df_commits (obj): Pandas dataframe of the commits
        df_mean (obj): Pandas dataframe of the commits mean values
        ax (obj): Matplotlib axe of the background

    Retuns:
        ax (obj): Matplotlib axe of the commits zoology
    """

    ax = sns.scatterplot(
        x="deletions",
        y="insertions",
        data=df_commits,
        color="black",
        s=100,
        alpha=0.3,
        legend=False,
    )
    ax = sns.scatterplot(
        x="mean_deletions",
        y="mean_insertions",
        data=df_mean,
        color="white",
        edgecolor="black",
        s=500 * np.sqrt(df_mean["commits_nbr"]),
        alpha=0.8,
        legend=False,
    )

    for i in range(df_mean.shape[0]):
        diameter = 500 * np.sqrt(df_mean.commits_nbr[i])
        char_size = min(20, 0.01 * diameter)
        plt.text(
            x=df_mean.mean_deletions[i],
            y=df_mean.mean_insertions[i],
            s=df_mean.author[i],
            fontdict=dict(color="black", size=char_size),
            verticalalignment="center",
            horizontalalignment="center",
        )

    plt.text(
        x=4e0,
        y=max(df_commits.insertions) * 0.02,
        s="Mostly insertions",
        fontdict=dict(color="black", size=25),
    )
    plt.text(
        x=max(df_commits.deletions) * 0.05,
        y=10,
        s="Mostly deletions",
        fontdict=dict(color="black", size=25),
    )
    plt.text(
        x=max(df_commits.deletions) * 0.08,
        y=max(df_commits.insertions) * 0.1,
        s="Refactoring",
        fontdict=dict(color="black", size=25),
    )

    ax.set_xlabel(xlabel="Lines deleted", fontsize=30)
    ax.set_ylabel(ylabel="Lines inserted", fontsize=30)
    ax.tick_params(axis="y", labelsize=30)
    ax.tick_params(axis="x", labelsize=30)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim([1, max(df_commits.deletions)])
    ax.set_ylim([1, max(df_commits.deletions)])

    # Changing y-axis and x-axis ticks

    y_formatter = FixedFormatter(["1", "10", "100", "1000", "10000", "100000"])
    y_locator = FixedLocator([1, 10, 100, 1000, 10000, 100000])
    ax.yaxis.set_major_formatter(y_formatter)
    ax.yaxis.set_major_locator(y_locator)

    x_formatter = FixedFormatter(["1", "10", "100", "1000", "10000", "100000"])
    x_locator = FixedLocator([1, 10, 100, 1000, 10000, 100000])
    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)

    return ax


def hist_churn(df_commits: pd.DataFrame) -> MplAxes:
    """
    Function that returns the hist plot matplotlib axe of the commits churn repartition

    Args:
        df_commits (obj): Pandas dataframe with the commits churn data.

    Returns:
        ax (obj): matplotlib.axes._axes object
    """

    f, ax = plt.subplots(figsize=(20, 15))
    f = sns.set_style("white")
    colors = [parameters[label] for label in df_commits.columns]

    f = df_commits.plot(kind="bar", stacked=True, color=colors, ax=ax)

    date_list = []
    date_loc = []
    for index in range(1, len(df_commits), 6):
        date_loc.append(int(index))
        date_list.append(df_commits.index[index].strftime("%d/%m/%y"))

    x_formatter = FixedFormatter(date_list)
    x_locator = FixedLocator(date_loc)
    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)

    ax.set_xlabel(xlabel="")
    ax.set_ylabel(ylabel="Number of lines", fontsize=30)
    ax.tick_params(axis="y", labelsize=30)
    ax.tick_params(axis="x", labelsize=20, rotation=45)
    ax.legend(fontsize=20)

    return ax


def blame_age_plot(df_blame: pd.DataFrame) -> MplAxes:
    """
    Function that returns the line plot matplotlib axe of the mean age of the lines

    Args:
        df_blame (obj): Pandas dataframe with the blame data.

    Returns:
        ax (obj): matplotlib.axes._axes object
    """
    df_blame_age = df_blame.groupby("date").agg(age=("age", "mean")).reset_index()

    f, ax = plt.subplots(figsize=(15, 10))
    f = sns.set_style("white")
    ax = sns.lineplot(x="date", y="age", data=df_blame_age)

    ax.set_xlabel(xlabel="Date", fontsize=25)
    ax.set_ylabel(ylabel="Age in months", fontsize=25)
    ax.tick_params(axis="y", labelsize=20)
    ax.tick_params(axis="x", labelsize=20)

    return ax


def historic_stackplot(
    df_code_age: pd.DataFrame, dict_code_age: dict, threshold: float
) -> list:
    """
    Function that returns the stackplot matplotlib axe of the commits churn repartition

    Note: ax here is a list of matplotlib axes since it's a stakcplot

    Args:
        df_code_age (obj): Pandas dataframe of the code age.
        dict_code_age (dict): dict with dates and coordinates
        threshold (float): Value between 0 and 1, fraction of the max_lines above which
                        the date will be written on the graph.

    Returns:
        ax (list): matplotlib.axes._axes object list
    """
    x_date = np.array(sorted(set(df_code_age["date"].to_numpy())))
    y_linedate = np.array(sorted(set(df_code_age.line_last_update)))
    colors = tol_cmap("rainbow_WhBr")(np.linspace(0, 1, len(dict_code_age)))

    fig, ax = plt.subplots(figsize=(20, 13))
    ax.stackplot(x_date, dict_code_age.values(), colors=colors, edgecolor="white")

    dates = sorted([date.strftime("%Y-%m") for date in y_linedate])
    date_value = [mdates.date2num(datetime.strptime(i, "%Y-%m")) for i in dates]
    color_bar = plt.cm.ScalarMappable(
        cmap=tol_cmap("rainbow_WhBr"),
        norm=plt.Normalize(vmin=date_value[0], vmax=date_value[-1]),
    )
    color_bar._A = []
    loc = mdates.AutoDateLocator()
    cbar = plt.colorbar(color_bar, ticks=loc, format=mdates.AutoDateFormatter(loc))

    date_to_write = get_lines_date(df_code_age, threshold)
    for date in date_to_write:
        plt.text(
            x=date_to_write[date][0],
            y=date_to_write[date][1],
            s=str(date),
            fontdict=dict(color="black", size=15),
        )

    # Adding labels, changing scales and limits
    ax.set_xlabel(xlabel="Date", fontsize=25)
    ax.set_ylabel(ylabel="Nbr of lines", fontsize=25)
    ax.tick_params(axis="y", labelsize=20)
    ax.tick_params(axis="x", labelsize=15)

    return ax


def get_author_names(path: str) -> set:
    """
    Function that returns a set with the trigrams of the authors

    Args:
        path (str): Path to the list_authors.json file

    Returns:
        names (set): Names of the authors as trigrams
    """
    names = []
    with open(path, "r") as read_file:
        for trigram in json.load(read_file).values():
            names.append(trigram)
    names = set(names)
    return names
