"""Module able to spawn several temporal versions of a git codebase"""

import json

import os
import shutil
import subprocess
from typing import List
from datetime import datetime, timedelta
from time import time

import yaml
from dateutil.relativedelta import relativedelta
from loguru import logger

from anubis.external_analysis import (
    run_tucan,
    rearrange_tucan_complexity_db,
)
from anubis.git_helpers import (
    git_blame,
    git_branch_status,
    git_checkout,
    git_last_revision,
    git_revision_in_between,
    git_revision_stats,
    git_tag_history,
    git_size,
)


def timeloop(
    branch: str,
    path: str,
    year_start: int,
    year_end: int,
    out_dir: str,
    mandatory_patterns: List[str] = None,
    forbidden_patterns: List[str] = None,
) -> None:
    """
    Principal time loop for anubis.

    Args :
        branch (str): Git branch to scan
        path (str): Path relative to git repo to limit the results
        year_start (int): Starting year, included
        year_end (int): Ending year, included
        out_dir (str): Output directory
        mandatory_patterns (List[str], optionnal): List of patterns to keep. Defaults to None.
        forbidden_patterns (List[str], optionnal): List of patterns to remove. Defaults to None.
    """

    total = (year_end - year_start + 1) * 12
    step = 0
    for year in range(year_start, year_end + 1):
        for month in range(12):
            raw_date = datetime(year, month + 1, 1, 12, 0)
            raw_date_m1 = raw_date + relativedelta(months=-1)
            if raw_date >= datetime.today():
                logger.info("Timemachine won't go in the future ... Stopping there")
                break

            logger.info(str(year) + "-" + str(month + 1))
            tstart = time()
            step += 1

            # first day of the month
            date = raw_date.isoformat()
            # first day of the month before
            date_m1 = raw_date_m1.isoformat()

            folder = f"{out_dir}/anubis_{year}-{month+1:02d}"
            logger.info(folder)

            if not os.path.isdir(folder):

                os.makedirs(folder)

                logger.info(f"Timewarping ({step}/{total})...  date is now " + date)
                revision = git_last_revision(date, branch)
                if revision is not None:
                    # checkout commit
                    git_checkout(revision)

                    revision_list_main = git_revision_in_between(
                        date_m1, date, branch=branch
                    )
                    revision_list_all = git_revision_in_between(date_m1, date)
                    # commits info
                    rev_stats = git_revision_stats(
                        branch, revision_list_main, revision_list_all
                    )
                    with open(f"{folder}/commits.json", "w") as fout:
                        json.dump(rev_stats, fout, indent=4)

                    # blame info
                    blame_info = git_blame(path)
                    with open(f"{folder}/blame.json", "w") as fout:
                        json.dump(blame_info, fout, indent=4)

                    # branch analysis
                    branch_info = git_branch_status(revision, date, branch)
                    with open(f"{folder}/branch_status.json", "w") as fout:
                        json.dump(branch_info, fout, indent=4)

                    # repo size
                    gitsize = git_size(path)
                    dict_size = {"size": gitsize}
                    with open(f"{folder}/git_size.json", "w") as fout:
                        json.dump(dict_size, fout, indent=4)

                    # Tucan analysis
                    tucan_struct = run_tucan(
                        path,
                        mandatory_patterns=mandatory_patterns,
                        forbidden_patterns=forbidden_patterns,
                    )
                    tucan_out = rearrange_tucan_complexity_db(tucan_struct)
                    with open(f"{folder}/complexity.json", "w") as fout:
                        json.dump(tucan_out, fout, indent=4)

                    tend = time()
                    duration = tend - tstart
                    eta = timedelta(seconds=(total - step) * duration)
                    logger.info(f"\nETA : {eta} sec")

    # Back to now
    sp = subprocess.run(["git", "checkout", revision], capture_output=True)

    # tag history
    tags_date = git_tag_history(branch)
    with open(f"{folder}/../tags_history.json", "w") as fout:
        json.dump(tags_date, fout, indent=4)


def main_timemachine(
    git_path: str,
    branch: str = "master",
    rel_source_path: str = "./",
    year_start: int = 2020,
    year_end: int = 2020,
    out_dir: str = "ANUBIS_OUT",
    mandatory_patterns: List[str] = None,
    forbidden_patterns: List[str] = None,
) -> None:
    """
    Main call of timemachine

    Args :
        git_path (str): Path to git repository
        branch (str, optionnal): Git branch to scan. Defaults to "master".
        rel_source_path (str, optionnal): Path relative to git repo to limit the results. Defaults to "./".
        year_start (int, optionnal): Starting year, included. Defaults to 2020.
        year_end (int, optionnal): Ending year, included. Defaults to 2020.
        out_dir (str, optionnal): Output directory. Defaults to "ANUBIS_OUT".
        mandatory_patterns (List[str], optionnal): List of patterns to keep. Defaults to None.
        forbidden_patterns (List[str], optionnal): List of patterns to remove. Defaults to None.
    """

    init_path = os.path.abspath(os.getcwd())
    full_dir = os.path.join(init_path, out_dir)

    if os.path.isdir(full_dir):
        pass
    else:
        os.makedirs(full_dir)

    f_log = os.path.join(init_path, out_dir, f"anubis_{os.getpid()}.log")
    logger.info(f_log)

    logger.info(full_dir)
    os.chdir(git_path)
    logger.info(f"Changing cwdir to {git_path} ")

    # Check if repo is clean before running timeloop
    sp = subprocess.run(["git", "status", "--porcelain"], capture_output=True)
    if not sp.stdout.decode():
        # repo is clean = stdout is empty
        timeloop(
            branch,
            rel_source_path,
            year_start,
            year_end,
            full_dir,
            mandatory_patterns=mandatory_patterns,
            forbidden_patterns=forbidden_patterns,
        )
        os.chdir(init_path)

        logger.info(f"Run log stored in {f_log} ")
    else:
        logger.error(f"error: Git repository is not clean:")
        logger.error(sp.stdout.decode())
        logger.error(
            f"Please, commit your changes or stash them before you can use Anubis timemachine.\nAborting"
        )


def run(inputfile="./anubis_time_machine.yml"):
    """
    Run anubis time machine from input file.

    Args:
        inputfile (str, optional): Path and name to the inputfile. Defaults to "./anubis_time_machine.yml".

    """
    logger.info(f"Start anubis from command line on {inputfile}\n")

    try:
        with open(inputfile, "r") as fin:
            param = yaml.load(fin, Loader=yaml.SafeLoader)
    except FileNotFoundError:
        logger.error(
            f'Input file {inputfile} not found, use the command "anubis anew" to create a new one'
        )
        raise

    main_timemachine(**param)
