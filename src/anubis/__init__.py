"""
Anubis
======

This is the root of the package Anubis, meant for preforming CodeMetrics Analyses on codebases.
"""

__version__ = "0.6.0"
