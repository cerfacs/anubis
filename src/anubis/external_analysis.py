"""External analysis for anubis"""

from typing import List
from loguru import logger

from tucan.package_analysis import run_struct
from tucan.struct_common import FIELDS_EXTENSIVE, FIELDS_INTENSIVE, FIELDS_SIZES

from tucan.travel_in_package import find_package_files_and_folders


def run_tucan(
    path: str,
    mandatory_patterns: List[str] = None,
    forbidden_patterns: List[str] = None,
    ignore_errors: bool = False,
) -> dict:
    """
    Main routine to do a structural analysis on a code file

    Args:
        path (str): Main path to start from
        optionnal_path_list (list): Optionnal path to look

    Returns:
        dict: Dict of the tucan structural analysis
    """
    logger.info("... running tucan")

    try:
        child_paths_dict = find_package_files_and_folders(
            path,
            mandatory_patterns=mandatory_patterns,
            forbidden_patterns=forbidden_patterns,
        )
    except FileNotFoundError:
        logger.error(f"Path: {path} doesn't exist in the repository at this date.")
        logger.error(
            "Either change the rel_source_path or add optional_source_path in the input file."
        )
        raise
    tucan_out = run_struct(child_paths_dict, ignore_errors=ignore_errors)
    return tucan_out


def rearrange_tucan_complexity_db(tucan_out: dict) -> dict:
    """
    Reformat the complexity database of tucan to be a better fit for anubis parsing
    and gathering of data.

    Args:
        tucan_out (dict): Dict of the tucan structural analysis

    Returns:
        dict: Rearranged tucan structural / complexity dict
    """
    fields = FIELDS_SIZES + FIELDS_EXTENSIVE + FIELDS_INTENSIVE
    fields.extend([f"{field}_int" for field in FIELDS_INTENSIVE])
    fields.extend([f"{field}_ext" for field in FIELDS_EXTENSIVE])

    # Initialize empty lists for each field
    tucan_db = {field: [] for field in fields}
    tucan_db.update(
        {"param": [], "size": [], "file": [], "function": [], "start": [], "end": []}
    )
    # Iterate over files and functions
    for file_name, functions in tucan_out.items():
        if functions:
            for func_name, func_data in functions.items():
                if not func_data["contains"]:
                    # Add field values to tucan_db
                    for field in fields:
                        tucan_db[field].append(func_data[field])

                    # Add additional fields
                    tucan_db["param"].append(1)  # Params default to 1
                    tucan_db["size"].append(
                        func_data["ssize"]
                    )  # TODO :move ev'rthg in anubis from size to ssize
                    tucan_db["file"].append(file_name)
                    tucan_db["function"].append(func_name)
                    tucan_db["start"].append(func_data["lines"][0])
                    tucan_db["end"].append(func_data["lines"][1])
    return tucan_db
